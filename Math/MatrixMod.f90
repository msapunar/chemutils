module matrixmod
    use fortutils
    implicit none

    private
    public :: genmat_diag
#if LINALG
    public :: mateig_sy
    public :: matexp_sy
    public :: matdet
#endif

contains


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: GenMat_Diag
    !> @brief Given a vector return diagonal matrix with values from vector on the diagonal.
    !----------------------------------------------------------------------------------------------
    function genmat_diag(vec) result(mat)
        real(dp), intent(in) :: vec(:)
        real(dp) :: mat(size(vec), size(vec))
        integer :: i

        mat = 0.0_dp
        do i = 1, size(vec)
            mat(i, i) = vec(i)
        end do
    end function genmat_diag



! The following subroutines require LAPACK with Fortran95 interface.
#if LINALG
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: MatEig_SY
    !
    !> @brief Eigensystem of a symmetric real matrix.
    !----------------------------------------------------------------------------------------------
    subroutine mateig_sy(a, evec, eval)
        use lapack95, only : syev
        real(dp), intent(in) :: a(:, :) !< Input matrix.
        real(dp), allocatable, intent(out) :: evec(:, :) !< Eigenvectors of the symmetric matrix.
        real(dp), allocatable, intent(out) :: eval(:) !< Eigenvalues of the symmetric matrix.
        integer :: n
        integer :: info

        n = size(a, 1)
        if (.not. allocated(evec)) allocate(evec(n, n))
        if (.not. allocated(eval)) allocate(eval(n))
 
        ! Calculate eigensystem.
        call syev(a=evec, w=eval, jobz='V', info=info)
        if (info /= 0) call errstop('MATEIG_SY', 'SYEV call failed.', info)
    end subroutine mateig_sy


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: MatExp_SY
    !
    !> @brief Matrix exponential of a symmetric real matrix multiplied by a complex factor.
    !> @details
    !! The subroutine first calculates the eigenvectors/eigenvalues of the matrix. Then the matrix
    !! exponantial is calculated in the eigenvector basis and the matrix is converted back to the
    !! original basis.
    !----------------------------------------------------------------------------------------------
    function matexp_sy(a, mult) result (res)
        use lapack95, only : syev
        use blas95, only : gemm
        real(dp), intent(in) :: a(:, :) !< Input matrix.
        complex(dp), intent(in) :: mult !< Multiplier in exponent.
        complex(dp), allocatable :: res(:, :) !< Result matrix.
        real(dp), allocatable :: evec(:, :) !< Eigenvectors of the symmetric matrix.
        real(dp), allocatable :: eval(:) !< Eigenvalues of the symmetric matrix.
        complex(dp), dimension(:, :), allocatable :: tmp1, tmp2 !< Work matrices
        integer :: i, n
        integer :: info

        n = size(a, 1)
        allocate(evec, source=a)
        allocate(eval(n))
        allocate(tmp1(n, n))
        allocate(tmp2(n, n))
        if (.not. allocated(res)) allocate(res(n, n))
 
        ! Calculate eigensystem.
        call syev(a=evec, w=eval, jobz='V', info=info)
        if (info /= 0) call errstop('MATEXP_SY', 'SYEV call failed.', info)

        ! Matrix exponential in eigenvector basis.
        res = cmplx(0.0_dp, 0.0_dp, dp)
        do i = 1, n
            res(i, i) = exp(eval(i) * mult)
        end do

        ! Conversion to original basis.
        tmp1 = cmplx(evec, 0.0_dp, dp)
        call gemm(tmp1, res, tmp2)
        call gemm(tmp2, tmp1, res, transb='C')
    end function matexp_sy


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: MatDet
    !> @brief Compute matrix determinant
    !> @details
    !! Use LAPACK LU factorization to compute the determinant. The input matrix is not changed.
    !----------------------------------------------------------------------------------------------
    pure function matdet(a) result(adet)
        use lapack95, only : getrf
        real(dp), intent(in) :: a(:, :) !< Matrix. (On exit it is in LU form.)
        real(dp) :: adet !< Determinant.
        real(dp), allocatable :: tmp(:, :) !< Work array.
        integer, allocatable :: ipiv(:)   ! pivot indices
        integer :: sgn !< Sign change based on pivots.
        integer :: info !< LAPACK exit status.
        integer :: i, n

        n = size(a, 1)
        allocate(tmp, source=a)
        allocate(ipiv(n))

        ! LU factorization using partial pivoting with row interchanges.
        call getrf(tmp, ipiv, info)
        if (info /= 0) then
            adet = 0.0_dp
            return
        end if

        sgn = 1
        adet = 1.0_dp
        do i = 1, n
            if (ipiv(i) /= i) sgn = -sgn
            adet = adet * tmp(i, i)
        end do
        adet = sgn * adet
    end function matdet
#endif


end module matrixmod
