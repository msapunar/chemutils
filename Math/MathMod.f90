MODULE mathmod
    use fortutils
    IMPLICIT NONE


CONTAINS


    SUBROUTINE calc_binom(n1,n2,binom)
        IMPLICIT NONE
        DOUBLE PRECISION, INTENT(OUT) :: binom
        INTEGER :: i
        INTEGER, INTENT(IN) :: n1, n2
        IF ((n1<2).OR.(n2==0).OR.(n2==n1)) THEN
            binom=1.D0
        ELSE
            IF ((n2==1).OR.(n2==n1-1)) THEN
                binom=n1*1.D0
            ELSE
                binom=1.D0
                DO i=1,n1
                    binom=binom*i
                END DO
                DO i=1,n2
                    binom=binom/i
                END DO
                DO i=1,n1-n2
                    binom=binom/i
                END DO
            END IF
        END IF
    END SUBROUTINE calc_binom


    SUBROUTINE beta_norm(nn,orb,z,b,b_out)
        IMPLICIT NONE
        INTEGER :: i, j, orb_sum, prod
        INTEGER, INTENT(IN) :: nn
        INTEGER, DIMENSION(1:3), INTENT(IN) :: orb
        DOUBLE PRECISION :: norm
        DOUBLE PRECISION, DIMENSION(1:nn), INTENT(IN) :: z, b
        DOUBLE PRECISION, DIMENSION(1:nn) :: b_N
        DOUBLE PRECISION, DIMENSION(1:nn), INTENT(OUT) :: b_out

        orb_sum=SUM(orb)
        norm=0.D0
        IF (orb_sum==0) THEN
            DO i=1,nn
                b_N(i)=b(i)*((2.D0*z(i))**0.75D0)
            END DO
            DO i=1,nn
                DO j=1,nn
                    norm=norm+b_N(i)*b_N(j)/DSQRT((z(i)+z(j))**3)
                END DO
            END DO
        ELSE
            prod=1
            DO i=1,3
                DO j=0,orb(i)
                    prod=prod*ABS(2*j-1)
                END DO
            END DO
            DO i=1,nn
                b_N(i)=b(i)*((2.D0)**(orb_sum*1.D0+0.75D0))*(z(i)**(orb_sum*0.5D0+0.75D0))/DSQRT(1.D0*prod)
            END DO
            DO i=1,nn
                DO j=1,nn
                    norm=norm+b_N(i)*b_N(j)/((z(i)+z(j))**(1.5D0+1.D0*orb_sum))
                END DO
            END DO
            norm=(norm*prod)/(2.D0**orb_sum)
        END IF
        IF (norm<=0.D0) STOP 'Error. Normalization constant less or equal to zero.'
        b_out=b_N/DSQRT(norm)
    END SUBROUTINE beta_norm

    !----------------------------------------------------------------------------------------------
    ! TYPE: Cross
    !> @brief Cross product of two 3D vectors.
    !----------------------------------------------------------------------------------------------
    pure function cross(v1, v2) result(v3)
        real(dp), intent(in) :: v1(3)
        real(dp), intent(in) :: v2(3)
        real(dp) :: v3(3)

        v3(1) = v1(2) * v2(3) - v1(3) * v2(2)
        v3(2) = v1(3) * v2(1) - v1(1) * v2(3)
        v3(3) = v1(1) * v2(2) - v1(2) * v2(1)
    end function cross


    SUBROUTINE jacobi(a,x,abserr,n)
    !===========================================================
    ! Evaluate eigenvalues and eigenvectors
    ! of a real symmetric matrix a(n,n): a*x = lambda*x
    ! method: Jacoby method for symmetric matrices
    ! Alex G. (December 2009)
    !-----------------------------------------------------------
    ! input ...
    ! a(n,n) - array of coefficients for matrix A
    ! n      - number of equations
    ! abserr - abs tolerance [sum of (off-diagonal elements)^2]
    ! output ...
    ! a(i,i) - eigenvalues
    ! x(i,j) - eigenvectors
    ! comments ...
    !===========================================================
        implicit none
        integer i, j, k, n
        double precision a(n,n),x(n,n)
        double precision abserr, b2, bar
        double precision beta, coeff, c, s, cs, sc
        ! initialize x(i,j)=0, x(i,i)=1
        ! *** the array operation x=0.0 is specific for Fortran 90/95
        x = 0.0
        do i=1,n
            x(i,i) = 1.0
         end do
        ! find the sum of all off-diagonal elements (squared)
        b2 = 0.0
        do i=1,n
            do j=1,n
                if (i.ne.j) b2 = b2 + a(i,j)**2
            end do
        end do
        if (b2 <= abserr) return
        ! average for off-diagonal elements /2
        bar = 0.5*b2/float(n*n)
        do while (b2.gt.abserr)
            do i=1,n-1
                do j=i+1,n
                    if (a(j,i)**2 <= bar) cycle  ! do not touch small elements
                    b2 = b2 - 2.0*a(j,i)**2
                    bar = 0.5*b2/float(n*n)
                    ! calculate coefficient c and s for Givens matrix
                    beta = (a(j,j)-a(i,i))/(2.0*a(j,i))
                    coeff = 0.5*beta/sqrt(1.0+beta**2)
                    s = sqrt(max(0.5_dp+coeff,0.0_dp))
                    c = sqrt(max(0.5_dp-coeff,0.0_dp))
                    ! recalculate rows i and j
                    do k=1,n
                        cs =  c*a(i,k)+s*a(j,k)
                        sc = -s*a(i,k)+c*a(j,k)
                        a(i,k) = cs
                        a(j,k) = sc
                    end do
                    ! new matrix a_{k+1} from a_{k}, and eigenvectors
                    do k=1,n
                        cs =  c*a(k,i)+s*a(k,j)
                        sc = -s*a(k,i)+c*a(k,j)
                        a(k,i) = cs
                        a(k,j) = sc
                        cs =  c*x(k,i)+s*x(k,j)
                        sc = -s*x(k,i)+c*x(k,j)
                        x(k,i) = cs
                        x(k,j) = sc
                    end do
                end do
            end do
        end do
        return
    END SUBROUTINE jacobi
        
    
!    FUNCTION inv(ain) result(c)
!        real(dp), dimension(:,:), intent(in) :: ain
!        real(dp), dimension(size(ain,1),size(ain,2)) :: c, a
!        real(dp), dimension(size(ain,1),size(ain,2)) :: L, U
!        real(dp), dimension(size(ain,1)) :: b, d, x
!        real(dp) :: coeff
!        integer :: n
!        integer :: i, j, k
!
!        ! step 0: initialization for matrices L and U and b
!        ! Fortran 90/95 aloows such operations on matrices
!        n=size(a,1)
!        a=ain
!        L=0.0
!        U=0.0
!        b=0.0
!
!        ! step 1: forward elimination
!        do k=1, n-1
!            do i=k+1,n
!                coeff=a(i,k)/a(k,k)
!                L(i,k) = coeff
!                do j=k+1,n
!                    a(i,j) = a(i,j)-coeff*a(k,j)
!                end do
!            end do
!        end do
!
!        ! Step 2: prepare L and U matrices
!        ! L matrix is a matrix of the elimination coefficient
!        ! + the diagonal elements are 1.0
!        do i=1,n
!            L(i,i) = 1.0
!        end do
!        ! U matrix is the upper triangular part of A
!        do j=1,n
!            do i=1,j
!                U(i,j) = a(i,j)
!            end do
!        end do
!
!        ! Step 3: compute columns of the inverse matrix C
!        do k=1,n
!            b(k)=1.0
!            d(1) = b(1)
!            ! Step 3a: Solve Ld=b using the forward substitution
!            do i=2,n
!                d(i)=b(i)
!                do j=1,i-1
!                    d(i) = d(i) - L(i,j)*d(j)
!                end do
!            end do
!            ! Step 3b: Solve Ux=d using the back substitution
!            x(n)=d(n)/U(n,n)
!            do i = n-1,1,-1
!                x(i) = d(i)
!                do j=n,i+1,-1
!                    x(i)=x(i)-U(i,j)*x(j)
!                end do
!                x(i) = x(i)/u(i,i)
!            end do
!            ! Step 3c: fill the solutions x(n) into column k of C
!            do i=1,n
!                c(i,k) = x(i)
!            end do
!            b(k)=0.0
!        end do
!
!    END FUNCTION inv


   ! FUNCTION det(mat_in) result(det_out)
   !     real(dp), dimension(:,:), intent(in) :: mat_in
   !     integer :: i, j, k, imax
   !     integer :: nn
   !     integer, dimension(1:size(mat_in,1)) :: indx
   !     real(dp) :: mat_max, dum, sum, det_out
   !     real(dp), dimension(1:size(mat_in,1),1:size(mat_in,2)) :: mat
   !     real(dp), dimension(1:10*size(mat_in,1)) :: vv
   !     real(dp), parameter :: tiny=1.5D-16
 
   !     nn = size(mat_in, 1)
   !     det_out=1.D0
   !     mat=mat_in
   !     DO i=1,nn
   !         mat_max=0.D0
   !         DO j=1,nn
   !             IF (ABS(mat(i,j))>mat_max) mat_max=ABS(mat(i,j))
   !         END DO
   !         IF (mat_max<tiny) THEN
   !             det_out=0.D0
   !             RETURN
   !         END IF
   !         vv(i)=1.D0/mat_max
   !     END DO
   !     DO j=1,nn
   !         DO i=1,j-1
   !             sum=mat(i,j)
   !             DO k=1,i-1
   !                 sum=sum-mat(i,k)*mat(k,j)
   !             END DO
   !             mat(i,j)=sum
   !         END DO
   !         mat_max=0.D0
   !         DO i=j,nn
   !             sum=mat(i,j)
   !             DO k=1,j-1
   !                 sum=sum-mat(i,k)*mat(k,j)
   !             END DO
   !             mat(i,j)=sum
   !             dum=vv(i)*ABS(sum)
   !             IF (dum>=mat_max) THEN
   !                 imax=i
   !                 mat_max=dum
   !             END IF
   !         END DO
   !         IF (j/=imax) THEN
   !             DO k=1,nn
   !                 dum=mat(imax,k)
   !                 mat(imax,k)=mat(j,k)
   !                 mat(j,k)=dum
   !             END DO
   !             det_out=-det_out
   !             vv(imax)=vv(j)
   !         END IF
   !         indx(j)=imax
   !         IF (ABS(mat(j,j))<tiny) mat(j,j)=tiny
   !         IF (j/=nn) THEN
   !             dum=1.D0/mat(j,j)
   !             DO i=j+1,nn
   !                 mat(i,j)=mat(i,j)*dum
   !             END DO
   !         END IF
   !     END DO
   !     DO i=1,nn
   !         det_out=det_out*mat(i,i)
   !     END DO
   ! END FUNCTION det


end module mathmod
