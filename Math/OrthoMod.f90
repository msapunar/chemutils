module orthomod
    use fortutils
    implicit none

    private
    public :: orth_gs
#if LINALG
    public :: orth_lowdin
#endif

contains

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Orth_GS
    !
    ! DESCRIPTION:
    !> @brief Modified Gram-Schmidt orthogonalization of a set of column vectors.
    !> @details
    !! Input is a real matrix with dimensions n x m matrix containing m vectors of dimension n.
    !! The vectors are orthogonalized (in place) using the Gram-Schmidt algorithm.
    !----------------------------------------------------------------------------------------------
    subroutine orth_gs(u)
        real(dp), intent(inout) :: u(:, :) !< Column vectors.
        integer :: j
        integer :: i

        u(:, 1) = u(:, 1) / norm(u(:, 1))
        do i = 2, size(u, 2)
            do j = 1, i - 1
                u(:, i) = u(:, i) - proj(u(:, i), u(:, j)) * u(:, j)
            end do
            u(:, i) = u(:, i) / norm(u(:, i))
        end do
    end subroutine orth_gs

    pure function proj(v1, v2) result(p)
        real(dp), intent(in) :: v1(:)
        real(dp), intent(in) :: v2(:)
        real(dp) :: p
        p = dot_product(v1, v2) / dot_product(v2, v2)
    end function proj

    pure function norm(v) result(n)
        real(dp), intent(in) :: v(:)
        real(dp) :: n
        n = sqrt(dot_product(v, v))
    end function norm


! The following subroutines require LAPACK with Fortran95 interface.
#if LINALG
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Orth_Lowdin
    !
    ! DESCRIPTION:
    !> @brief Lowdin (symmetric) orthogonalization of a set of column vectors.
    !> @details
    !! Input is a real matrix with dimensions n x m matrix containing m vectors of dimension n.
    !! The vectors are orthogonalized (in place) using singular value decomposition.
    !----------------------------------------------------------------------------------------------
    subroutine orth_lowdin(mat)
        use lapack95, only : gesvd
        use blas95, only : gemm
        real(dp), intent(inout) :: mat(:, :) !< Column vectors.
        real(dp), allocatable :: s(:) !< Singular values (not used.)
        real(dp), allocatable :: vt(:, :) !< Transposed V matrix.
        real(dp), allocatable :: u(:, :) !< U matrix.
        integer :: m, n !< Array size.
        integer :: info !< LAPACK return code.

        m = size(mat, 1)
        n = size(mat, 2)
        allocate(s(min(m, n)))
        allocate(u(m, m))
        allocate(vt(n, n))
        
        ! Compute SVD
        call gesvd(mat, s, u=u, vt=vt, info=info)
        if (info /= 0) call errstop('ORTH_LOWDIN', 'GESVD call failed.', info)

        ! Overwrite input matrix with orthogonized matrix U.Vt.
        call gemm(u, vt, mat)
    end subroutine orth_lowdin
#endif

end module orthomod
