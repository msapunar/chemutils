!--------------------------------------------------------------------------------------------------
! MODULE: OneElOpMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date November, 2016
!
! DESCRIPTION: 
!> @brief Contains the subroutine for computing a 1 el. operator over two sets of basis functions.
!--------------------------------------------------------------------------------------------------
module oneelopmod
    ! Import variables
    use fortutils
    implicit none

    private
    public :: oneelop
    public :: cartoneelop


contains

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: OneElOp
    !
    ! DESCRIPTION:
    !> @brief Matrix of a simple one electron operator over two sets of basis functions.
    !> @details
    !! Calculates a matrix with elements:
    !!
    !!     S_12(i,j) = < B_1,i | x^lx*y^ly*z^lz | B_2,j >
    !!
    !! The matrix is initially constructed in the basis of contracted Cartesian Gaussian functions 
    !! (given in the ao_c1 and ao_c2 variables). Afterwards, the basis is changed using the left
    !! and right transformation matrices.
    !----------------------------------------------------------------------------------------------
    subroutine oneelop(ao_c1, ao_c2, lxyz, trans1, trans2, mat)
        use ccgmod
        type(ccg), intent(in) :: ao_c1(:) !< First basis set.
        type(ccg), intent(in) :: ao_c2(:) !< Second basis set.
        integer, intent(in) :: lxyz(3) !< List of exponents of the coordinates in the operator.
                                       !! (lx, ly, lz)
        real(dp), intent(in) :: trans1(:, :) !< First basis transformation matrix.
        real(dp), intent(in) :: trans2(:, :) !< Second basis transformation matrix.
        real(dp), allocatable, intent(out) :: mat(:, :) !< Operator matrix.
        real(dp), allocatable :: cmat(:, :)
        real(dp), allocatable :: tmat(:, :)
        integer :: nc1, nc2, nb1, nb2

        nc1 = size(ao_c1)
        nc2 = size(ao_c2)
        nb1 = size(trans1, 1)
        nb2 = size(trans2, 1) 

        call cartoneelop(ao_c1, ao_c2, lxyz, cmat)

        if (.not. allocated(mat)) allocate(mat(nb1, nb2))
        allocate(tmat(nb1, nc2))
        tmat = matmul(trans1, cmat)
        mat = matmul(tmat, transpose(trans2))
        deallocate(tmat)
        deallocate(cmat)
    end subroutine oneelop


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: CartOneElOp
    !
    ! DESCRIPTION:
    !> @brief Matrix of a simple one electron operator over two sets of cartesian basis functions.
    !> @details
    !! Calculates a matrix with elements:
    !!
    !!     S_12(i,j) = < B_1,i | x^lx*y^ly*z^lz | B_2,j >
    !!
    !! where < B_1,i | and | B_2,j > are contracnted Cartesian Gaussian functions. 
    !----------------------------------------------------------------------------------------------
    subroutine cartoneelop(ao_c1, ao_c2, lxyz, mat)
        use ccgmod
        type(ccg), intent(in) :: ao_c1(:) !< First basis set.
        type(ccg), intent(in) :: ao_c2(:) !< Second basis set.
        integer, intent(in) :: lxyz(3) !< List of exponents of the coordinates in the operator.
                                       !! (lx, ly, lz)
        real(dp), allocatable, intent(out) :: mat(:, :) !< Operator matrix.
        real(dp), parameter :: PiTo3Over2 = 5.5683279968317078_dp
        real(dp), parameter :: tinydp = 0.00000000000001_dp

        integer :: i
        integer :: j
        integer :: k
        integer :: l
        integer :: q
        real(dp) :: ssum
        real(dp) :: z1
        real(dp) :: z2
        real(dp) :: p
        real(dp) :: sprod
        real(dp) :: q1
        real(dp) :: q2
        real(dp) :: qp
        real(dp) :: sq
        real(dp) :: r2

        if (.not. allocated(mat)) allocate(mat(size(ao_c1, 1), size(ao_c2, 1)))
        mat = 0.0_dp

        do i = 1, size(ao_c1, 1)
            do j = 1, size(ao_c2, 1)
                ssum = 0.0_dp
                do k = 1, ao_c1(i)%npr
                    z1 = ao_c1(i)%z(k)
                    do l = 1, ao_c2(j)%npr
                        z2 = ao_c2(j)%z(l)
                        p = z1 + z2
                        sprod = 1.0_dp
                        do q = 1, 3
                            q1 = ao_c1(i)%q0(q)
                            q2 = ao_c2(j)%q0(q)
                            qp = (z1*q1 + z2*q2) / (z1+z2)
                            call osrec ([ao_c1(i)%ang(q),ao_c2(j)%ang(q),lxyz(q)], &
                                        [q1,q2,0.0_dp], qp, 0.5_dp/p, sq)
                            sprod = sprod * sq
                        end do
                        r2 = sum((ao_c1(i)%q0 - ao_c2(j)%q0)**2)
                        ssum = ssum + ao_c1(i)%b(k) * ao_c2(j)%b(l) * sprod * PiTo3over2 &
                               * p ** (-1.5_dp) * exp(-(z1 * z2) * r2 / p)
                    end do
               end do
               if (abs(ssum) > tinydp) mat(i,j) = ssum
            end do
        end do
    end subroutine cartoneelop


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: OSRec
    !
    ! DESCRIPTION:
    !> @brief Obara-Saika recurrence relations for calculating integrals.
    !> @details
    !! The Obara-Saika recurrence relations for calculating the overlap and multipole integrals of
    !! Cartesian Gaussian functions of arbitrary quantum number:
    !! G(a,q,q0,lq) = (q-q0)^lq * exp(-a * (q-q0)^2)
    !! Integral: S(lq1, lqb,lq3) = < G(a,q,q0a,lqa) | (q-q03)^lq3 | G(b,q,q0b,lqb) >
    !! Recurrence relations:
    !!      S(i+1,j,k) = (qp-q0a) * S(i,j,k) + (i*S(i-1,j,k) + j*S(i,j-1,k) + k*S(i,j,k-1))/2(a+b)
    !!      S(i,j+1,k) = (qp-q0b) * S(i,j,k) + (i*S(i-1,j,k) + j*S(i,j-1,k) + k*S(i,j,k-1))/2(a+b)
    !!      S(i,j,k+1) = (qp-q03) * S(i,j,k) + (i*S(i-1,j,k) + j*S(i,j-1,k) + k*S(i,j,k-1))/2(a+b)
    !
    !> @param inl - The 3 quantum numbers: lqa, lqb and lq3. 
    !> @param inq - Coordinates of the centres of the functions: q0a, q0b, q03
    !> @param qp - Position of the Gaussian overlap distribution: qp = (lqa*q0a + lqb*q0b)/(a+b)
    !> @param inv2p - 1/(2(a+b))
    !> @param sout - Result of applying the recurrence relations up to S(lqa,lqb,lq3). 
    !!               For the value of the integral this should be multiplied by S(0,0,0).
    !----------------------------------------------------------------------------------------------
    subroutine osrec(inl, inq, qp, inv2p, sout)
        integer,intent(in) :: inl(3)
        real(dp),intent(in) :: inq(3)
        real(dp),intent(in) :: qp
        real(dp),intent(in) :: inv2p
        real(dp),intent(out) :: sout
        integer :: i
        integer :: j
        integer :: k
        integer :: l1
        integer :: l2
        integer :: l3
        integer :: ord(3)
        real(dp) :: q1
        real(dp) :: q2
        real(dp) :: q3
        real(dp) :: qp1
        real(dp) :: qp2
        real(dp) :: qp3
        real(dp), allocatable :: s(:,:,:)

        if (inl(1)>=inl(2) .and. inl(1)>=inl(3)) then
            if (inl(2)>=inl(3)) then
                ord=[1,2,3]
            else
                ord=[1,3,2]
            end if
        else if (inl(2)>=inl(3)) then
            if (inl(1)>=inl(3)) then
                ord=[2,1,3]
            else
                ord=[2,3,1]
            end if
        else
            if (inl(1)>=inl(2)) then
                ord=[3,1,2]
            else
                ord=[3,2,1]
            end if
        end if
        l1=inl(ord(1))
        l2=inl(ord(2))
        l3=inl(ord(3))
        q1=inq(ord(1))
        q2=inq(ord(2))
        q3=inq(ord(3))

        allocate(s(0:l1,0:l2,0:l3))
        s = 0.0_dp
        qp1 = qp - q1
        qp2 = qp - q2
        qp3 = qp - q3
        s(0,0,0) = 1.0_dp
        if (l1>0) s(1,0,0) = qp1
        if (l2>0) then
            s(0,1,0) = qp2
            s(1,1,0) = qp1 * qp2 + inv2p
        end if
        do i=2,l1
            s(i,0,0) = qp1*s(i-1,0,0) + inv2p*(i-1)*s(i-2,0,0)
        end do
        if (l2>0) then
            do i=2,l1
                s(i,1,0) = qp2*s(i,0,0) + inv2p * i *s(i-1,0,0)
            end do
        end if
        do j=2,l2
            s(0,j,0) = qp1*s(0,j-1,0) + inv2p*(j-1)*s(0,j-2,0)
            s(1,j,0) = qp2*s(0,j,0)   + inv2p* j   *s(0,j-1,0)
        end do
        do j=2,l2
            do i=2,l1
                s(i,j,0) = qp2*s(i,j-1,0) + inv2p * (i*s(i-1,j-1,0) + (j-1)*s(i,j-2,0))
            end do
        end do
        do j=l2-l3+1,l2
            do i=l3-l2+j,l1
                s(i,j,1) = qp3*s(i,j,0) + inv2p * (i*s(i-1,j,0) + j*s(i,j-1,0))
            end do
        end do
        do k=2,l3
            do j=l2-l3+k,l2
                do i=l3-l2+j-k+1,l1
                    s(i,j,k)=qp3*s(i,j,k-1) + inv2p * (i*s(i-1,j,k-1) + j*s(i,j-1,k-1) + (k-1)*s(i,j,k-2))
                end do
            end do
        end do

        sout = s(l1,l2,l3)
        deallocate(s)
    end subroutine osrec


end module oneelopmod
