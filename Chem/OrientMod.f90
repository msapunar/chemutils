!----------------------------------------------------------------------------------------------
! MODULE: OrientMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date October, 2017
!
! DESCRIPTION:
!> @brief 
!----------------------------------------------------------------------------------------------
module orientmod
    ! Import variables
    use fortutils
    use geometrymod
    implicit none

    private
    public :: orient_t2com
    public :: orient_inertmat
!   public :: orient_inertaxes

contains

    
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Orient_t2com
    !> @brief Translate geometry to center of mass.
    !----------------------------------------------------------------------------------------------
    subroutine orient_t2com(mass, geom)
        real(dp), intent(in) :: mass(:)
        real(dp), intent(inout) :: geom(:, :)
        real(dp) :: c(size(geom, 1))
        integer :: i

        c = geom_com(mass, geom)
        do i = 1, size(geom, 1)
            geom(i, :) = geom(i, :) - c(i)
        end do
    end subroutine orient_t2com


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Orient_inertmat
    !> @brief Calculate inertia matrix.
    !----------------------------------------------------------------------------------------------
    pure function orient_inertmat(mass, geom) result(iner)
        real(dp), intent(in) :: mass(:)
        real(dp), intent(in) :: geom(:, :)
        real(dp) :: iner(3, 3)
        integer :: i

        iner = 0.0_dp
        do i = 1, size(mass)
            iner(1, 1) = iner(1, 1) + mass(i) * (geom(2, i)**2 + geom(3, i))
            iner(2, 2) = iner(2, 2) + mass(i) * (geom(1, i)**2 + geom(3, i))
            iner(3, 3) = iner(3, 3) + mass(i) * (geom(1, i)**2 + geom(2, i))
            iner(1, 2) = iner(1, 2) - mass(i) * geom(1, i) * geom(2,i)
            iner(1, 3) = iner(1, 3) - mass(i) * geom(1, i) * geom(3,i)
            iner(2, 3) = iner(2, 3) - mass(i) * geom(2, i) * geom(3,i)
        end do
        iner(2,1) = iner(1,2)
        iner(3,1) = iner(1,3)
        iner(3,2) = iner(2,3)
    end function orient_inertmat


!   !----------------------------------------------------------------------------------------------
!   ! SUBROUTINE: Orient_inertaxes
!   !> @brief Calculate the pricipal axes of inertia for a system.
!   !> @todo Orient axes in standard direction when there are degenerate eigenvalues.
!   !----------------------------------------------------------------------------------------------
!   subroutine orient_inertaxes(mass, geom, axes, values)
!       use matrixmod, only : mateig_sy
!       real(dp), intent(in) :: mass(:) !< List of masses.
!       real(dp), intent(in) :: geom(:, :) !< Positions of atoms.
!       real(dp), intent(out) :: axes(3, 3) !< Principal axes of inertia.
!       real(dp), intent(out) :: values(3)
!       real(dp) :: imat(3, 3) !< Inertia matrix.

!       imat = orient_inertmat(mass, geom)
!       call mateig_sy(imat, axes, values)

!   end subroutine orient_inertaxes



end module orientmod
