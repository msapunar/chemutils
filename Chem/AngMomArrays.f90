!----------------------------------------------------------------------------------------------
! MODULE: AngMomArrays
!
! DESCRIPTION:
!> @brief
!! Module with helper arrays for basis set definitions and transformations.
!> @details
!! Contains arrays with the angular momenta of gaussian basis functions, and transformation 
!! arrays for switching between spherical and cartesian gaussian functions.
!----------------------------------------------------------------------------------------------
module angmomarrays
    use fortutils
    
    integer, parameter :: PAngMom(3,3) = reshape( &
          [1, 0, 0, &
           0, 1, 0, &
           0, 0, 1], &
          shape(PAngMom), order=[2,1])
 
    integer, parameter :: DAngMom(3,6) = reshape( &
          [2, 0, 0, 1, 1, 0, &
           0, 2, 0, 1, 0, 1, &
           0, 0, 2, 0, 1, 1], &
          shape(DAngMom), order=[2,1])
 
    integer, parameter :: FAngMom(3,10) = reshape( &
          [3, 0, 0, 2, 2, 1, 0, 1, 0, 1, &
           0, 3, 0, 1, 0, 2, 2, 0, 1, 1, &
           0, 0, 3, 0, 1, 0, 1, 2, 2, 1], &
          shape(FAngMom), order=[2,1])
 
    integer, parameter :: GAngMom(3,15) = reshape( &
          [4, 0, 0, 3, 3, 1, 0, 1, 0, 2, 2, 0, 2, 1, 1, &
           0, 4, 0, 1, 0, 3, 3, 0, 1, 2, 0, 2, 1, 2, 1, &
           0, 0, 4, 0, 1, 0, 1, 3, 3, 0, 2, 2, 1, 1, 2], &
          shape(GAngMom), order=[2,1])
 
    integer, parameter :: HAngMom(3,21) = reshape( &
          [5, 0, 0, 4, 4, 1, 0, 1, 0, 3, 3, 2, 0, 2, 0, 3, 1, 1, 2, 2, 1, &
           0, 5, 0, 1, 0, 4, 4, 0, 1, 2, 0, 3, 3, 0, 2, 1, 3, 1, 2, 1, 2, &
           0, 0, 5, 0, 1, 0, 1, 4, 4, 0, 2, 0, 2, 3, 3, 1, 1, 3, 1, 2, 2], &
          shape(HAngMom), order=[2,1])
 
    real(dp), parameter :: PTransform(3,3) = reshape( &
          [1.0_dp, 0.0_dp, 0.0_dp, &
           0.0_dp, 1.0_dp, 0.0_dp, &
           0.0_dp, 0.0_dp, 1.0_dp], &
          shape(PTransform), order=[2,1])
 
    real(dp), parameter :: DTransform(5,6) = reshape( &
          [ -0.5_dp, -0.5_dp, 1.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 1.0_dp, 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 1.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 1.0_dp, 0.0_dp, 0.0_dp, &
          sqrt(3.0_dp/4.0_dp), -sqrt(3.0_dp/4.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp], &
          shape(DTransform), order=[2,1])
 
    real(dp), parameter :: FTransform(7,10) = reshape( &
          [0.0_dp, 0.0_dp, 1.0_dp, 0.0_dp, -sqrt(9.0_dp/20.0_dp), 0.0_dp, -sqrt(9.0_dp/20.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, &
          -sqrt(3.0_dp/8.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(3.0_dp/40.0_dp), 0.0_dp, sqrt(6.0_dp/5.0_dp), 0.0_dp, 0.0_dp, &
          0.0_dp, -sqrt(3.0_dp/8.0_dp), 0.0_dp, -sqrt(3.0_dp/40.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(6.0_dp/5.0_dp), 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 1.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(3.0_dp/4.0_dp), 0.0_dp, -sqrt(3.0_dp/4.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, &
          sqrt(5.0_dp/8.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(9.0_dp/8.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, &
          0.0_dp, sqrt(5.0_dp/8.0_dp), 0.0_dp, -sqrt(9.0_dp/8.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp], &
          shape(FTransform), order=[2,1])
 
    real(dp), parameter :: GTransform(9,15) = reshape( &
          [3.0_dp/8.0_dp, 3.0_dp/8.0_dp, 1.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(27.0_dp/560.0_dp), -sqrt(27.0_dp/35.0_dp), -sqrt(27.0_dp/35.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(45.0_dp/56), 0.0_dp, 0.0_dp, sqrt(10.0_dp/7.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(9.0_dp/56.0_dp), 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(45.0_dp/56), 0.0_dp, sqrt(10.0_dp/7.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(9.0_dp/56.0_dp), 0.0_dp, 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, -sqrt(5.0_dp/28.0_dp), 0.0_dp, -sqrt(5.0_dp/28.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(9.0_dp/7.0_dp), &
          sqrt(5.0_dp/16.0_dp), -sqrt(5.0_dp/16.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(27.0_dp/28.0_dp), sqrt(27.0_dp/28.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(5.0_dp/8.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(9.0_dp/8.0_dp), 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(5.0_dp/8.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(9.0_dp/8.0_dp), 0.0_dp, 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, sqrt(5.0_dp/4.0_dp), 0.0_dp, -sqrt(5.0_dp/4.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, &
          sqrt(35.0_dp/64.0_dp), sqrt(35.0_dp/64.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(27.0_dp/16.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp], &
          shape(GTransform), order=[2,1])
 
    real(dp), parameter :: HTransform(11,21) = reshape( &
          [0.0_dp, 0.0_dp, 1.0_dp, 0.0_dp, 5.0_dp/8.0_dp, 0.0_dp, 5.0_dp/8.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(25.0_dp/21.0_dp), -sqrt(25.0_dp/21.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, sqrt(15.0_dp/112.0_dp), 0.0_dp, 0.0_dp, &
          sqrt(15.0_dp/64.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(5.0_dp/192.0_dp), 0.0_dp, sqrt(5.0_dp/3.0_dp), 0.0_dp, sqrt(5.0_dp/112.0_dp), -sqrt(45.0_dp/28.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(9.0_dp/28.0_dp), &
          0.0_dp, sqrt(15.0_dp/64.0_dp), 0.0_dp, sqrt(5.0_dp/192.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(5.0_dp/3.0_dp), 0.0_dp, 0.0_dp, sqrt(5.0_dp/112.0_dp), -sqrt(45.0_dp/28.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(9.0_dp/28.0_dp), 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(5.0_dp/12.0_dp), -sqrt(5.0_dp/12.0_dp), sqrt(5.0_dp/3.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(35.0_dp/48.0_dp), 0.0_dp, sqrt(35.0_dp/48.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(5.0_dp/4.0_dp), -sqrt(5.0_dp/4.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, &
          -sqrt(35.0_dp/128.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(35.0_dp/128.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, sqrt(5.0_dp/96.0_dp), sqrt(5.0_dp/6.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(3.0_dp/2.0_dp), &
          0.0_dp, sqrt(35.0_dp/128.0_dp), 0.0_dp, -sqrt(35.0_dp/128.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(5.0_dp/96.0_dp), -sqrt(5.0_dp/6.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(3.0_dp/2.0_dp), 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(5.0_dp/4.0_dp), -sqrt(5.0_dp/4.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, &
          0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(35.0_dp/64.0_dp), 0.0_dp, sqrt(35.0_dp/64.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(27.0_dp/16.0_dp), 0.0_dp, 0.0_dp, &
          sqrt(63.0_dp/128.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, sqrt(175.0_dp/128.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(75.0_dp/32.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, &
          0.0_dp, sqrt(63.0_dp/128.0_dp), 0.0_dp, sqrt(175.0_dp/128.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, -sqrt(75.0_dp/32.0_dp), 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp, 0.0_dp], &
          shape(HTransform), order=[2,1])

end module angmomarrays
