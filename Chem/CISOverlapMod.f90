!--------------------------------------------------------------------------------------------------
! MODULE: CISOverlapMod
!
! DESCRIPTION: 
!--------------------------------------------------------------------------------------------------
module cisoverlapmod
    ! Import variables
    use fortutils
    ! Import subroutines
    use matrixmod
    implicit none
    
    private
    public :: cis_olap_full


contains


    subroutine cis_olap_full(csc, no, nv1, nv2, wf1, wf2, omat)
        real(dp), intent(in) :: csc(:, :, :) ! MO overlap matrix.
        integer, intent(in) :: no(:) !< Number of occupied orbitals.
        integer, intent(in) :: nv1(:) !< Number of virtual orbitals (bra).
        integer, intent(in) :: nv2(:) !< Number of virtual orbitals (ket).
        type(rmat), intent(inout) :: wf1(:) !< Ket wave function coefficients.
        type(rmat), intent(inout) :: wf2(:) !< Bra wave function coefficients.
        real(dp), allocatable, intent(out) :: omat(:, :) !< Overlap matrix.
        real(dp), allocatable :: rr(:)
        real(dp), allocatable :: rs(:, :)
        real(dp), allocatable :: sr(:, :)
        real(dp), allocatable :: ss(:, :, :)
        integer :: rhf, rhf1, rhf2, ns1, ns2, s1, s2, s
        integer :: st1, st2

        rhf = size(csc, 3) 
        rhf1 = size(wf1)
        rhf2 = size(wf2)
        ns1 = size(wf1(1)%c, 2)
        ns2 = size(wf2(1)%c, 2)
        allocate(rr(rhf))
        allocate(rs(ns2, rhf))
        allocate(sr(ns1, rhf))
        allocate(ss(ns1, ns2, rhf))
        if (.not. allocated(omat)) allocate(omat(ns1 + 1, ns2 + 1))

        if (rhf == 2) then
            ! Renormalize wave functions if they have different number of spin functions:
            if (rhf1 == 1) wf1(1)%c = wf1(1)%c / sqrt(2.0_dp)
            if (rhf2 == 1) wf2(1)%c = wf2(1)%c / sqrt(2.0_dp)
        end if

        do s = 1, rhf
            s1 = min(s, rhf1) 
            s2 = min(s, rhf2)
            ! Calculate blocks:
            call rrblock(csc(:, :, s), no(s), rr(s))
            call rsblock(csc(:, :, s), no(s), nv2(s2), ns2, wf2(s2)%c, rs(:, s))
            call srblock(csc(:, :, s), no(s), nv1(s1), ns1, wf1(s1)%c, sr(:, s))
            call ssblock(csc(:, :, s), no(s), nv1(s1), nv2(s2), ns1, ns2, wf1(s1)%c, wf2(s2)%c,    &
            &            ss(:, :, s))
        end do

        if (rhf == 1) then
            ! Ground state overlap:
            omat(1, 1) = rr(1) * rr(1)
            ! Ground-excited state overlap:
            do st1 = 1, ns1
                omat(st1 + 1, 1) = rr(1) * sr(st1, 1)
            end do
            ! Excited-ground state overlap:
            do st2 = 1, ns2
                omat(1, st2 + 1) = rr(1) * rs(st2, 1)
            end do
            ! Excited-excited state overlap:
            do st1 = 1, ns1 
                do st2 = 1, ns2 
                    omat(st1 + 1, st2 + 1) = rr(1) * ss(st1, st2, 1) + sr(st1, 1) * rs(st2, 1)
                end do
            end do
        else
            ! Ground state overlap:
            omat(1, 1) = rr(1) * rr(2)
            ! Ground-excited state overlap:
            do st1 = 1, ns1
                omat(st1 + 1, 1) = rr(1) * sr(st1, 2) + rr(2) * sr(st1, 1)
            end do
            ! Excited-ground state overlap:
            do st2 = 1, ns2
                omat(1, st2 + 1) = rr(2) * rs(st2, 1) + rr(1) * rs(st2, 2)
            end do
            ! Excited-excited state overlap:
            do st1 = 1, ns1
                do st2 = 1, ns2
                    omat(st1 + 1, st2 + 1) = rr(2) * ss(st1, st2, 1) + sr(st1, 1) * rs(st2, 2)     &
                    &                      + rr(1) * ss(st1, st2, 2) + sr(st1, 2) * rs(st2, 1)
                end do
            end do
        end if

    end subroutine cis_olap_full
 

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: RRBlock
    !> @brief Reference determinant overlap.
    !----------------------------------------------------------------------------------------------
    subroutine rrblock(csc, no, rr)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        real(dp), intent(out) :: rr !< Determinant.
        rr = matdet(csc(1:no, 1:no))
    end subroutine rrblock


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SRBlock
    !> @brief Overlaps of singly excited bra determinants with ket reference determinant.
    !----------------------------------------------------------------------------------------------
    subroutine srblock(csc, no, nv1, ns1, wf1, sr)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        integer, intent(in) :: nv1 !< Number of bra virtual orbitals.
        integer, intent(in) :: ns1 !< Number of bra states.
        real(dp), intent(in) :: wf1(no*nv1, ns1) !< Bra wf coefficients.
        real(dp), intent(out) :: sr(ns1) 

        integer :: o
        integer :: v
        integer :: st
        real(dp) :: cdet
       !real(dp) :: ref(1:no, 1:no)
       !real(dp) :: tmp(1:no, 1:no) ! Bug when defined like this. With ifort the loop would
                                    ! finish but the final values of sr wouldn't be saved.
        real(dp), allocatable :: ref(:, :)
        real(dp), allocatable :: tmp(:, :)
        allocate(ref(1:no, 1:no))
        allocate(tmp(1:no, 1:no))

        ref = csc(1:no, 1:no)
        sr = 0.0_dp
        !$omp parallel shared (sr)
        !$omp do private(tmp, cdet) schedule(dynamic) reduction(+:sr)
        do o = 1, no
            tmp = ref
            do v = 1, nv1
                tmp(o, :) = csc(no + v, 1:no)
                cdet = matdet(tmp)
                do st = 1, ns1
                    sr(st) = sr(st) + cdet * wf1((o-1)*nv1 + v, st)
                end do
            end do
        end do
        !$omp end do
        !$omp end parallel
    end subroutine srblock


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: RSBlock
    !> @brief Overlaps of bra reference determinant with singly excited ket determinants.
    !----------------------------------------------------------------------------------------------
    subroutine rsblock(csc, no, nv2, ns2, wf2, rs)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        integer, intent(in) :: nv2 !< Number of ket virtual orbitals.
        integer, intent(in) :: ns2 !< Number of ket states.
        real(dp), intent(in) :: wf2(no*nv2, ns2) !< Ket wf coefficients.
        real(dp), intent(out) :: rs(ns2)

        integer :: o
        integer :: v
        integer :: st
        real(dp) :: cdet
       !real(dp) :: ref(1:no, 1:no)
       !real(dp) :: tmp(1:no, 1:no) ! Bug when defined like this. With ifort the loop would
                                    ! finish but the final values of rs wouldn't be saved.
        real(dp), allocatable :: ref(:, :)
        real(dp), allocatable :: tmp(:, :)

        allocate(ref(1:no, 1:no))
        allocate(tmp(1:no, 1:no))
        ref = csc(1:no, 1:no)
        rs = 0.0_dp
        !$omp parallel shared(rs)
        !$omp do private(tmp, cdet) schedule(dynamic) reduction(+:rs)
        do o = 1, no
            tmp = ref
            do v = 1, nv2
                tmp(:, o) = csc(1:no, no + v)
                cdet = matdet(tmp)
                do st = 1, ns2
                    rs(st) = rs(st) + cdet * wf2((o-1)*nv2 + v, st)
                end do
            end do
        end do
        !$omp end do
        !$omp end parallel
    end subroutine rsblock

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SSBlock
    !> @brief Overlaps of singly excited bra and ket determinants.
    !----------------------------------------------------------------------------------------------
    subroutine ssblock(csc, no, nv1, nv2, ns1, ns2, wf1, wf2, ss)
        real(dp), intent(in) :: csc(:, :) !< Molecular orbital overlap matrix.
        integer, intent(in) :: no !< Number of occupied orbitals.
        integer, intent(in) :: nv1 !< Number of bra virtual orbitals.
        integer, intent(in) :: nv2 !< Number of ket virtual orbitals.
        integer, intent(in) :: ns1 !< Number of bra states.
        integer, intent(in) :: ns2 !< Number of ket states.
        real(dp), intent(in) :: wf1(no*nv1, ns1) !< Bra wf coefficients.
        real(dp), intent(in) :: wf2(no*nv2, ns2) !< Ket wf coefficients.
        real(dp), intent(out) :: ss(ns1, ns2)

        integer :: o1
        integer :: o2
        integer :: v1
        integer :: v2
        integer :: st1
        integer :: st2
        real(dp) :: cdet
       !real(dp) :: ref(1:no, 1:no)
       !real(dp) :: tmp(1:no, 1:no)
       !real(dp) :: tmp2(1:no, 1:no) ! Bug when defined like this. With ifort the loop would
                                     ! finish but the final values of ss wouldn't be saved.
        real(dp), allocatable :: ref(:, :)
        real(dp), allocatable :: tmp(:, :)
        real(dp), allocatable :: tmp2(:, :)

        allocate(ref(1:no, 1:no))
        allocate(tmp(1:no, 1:no))
        allocate(tmp2(1:no, 1:no))

        ss = 0.0_dp
        ref = csc(1:no, 1:no)
        !$omp parallel shared(ss)
        !$omp do private(tmp, tmp2, cdet) schedule(dynamic) reduction(+:ss)
        do o1 = 1, no
          tmp = ref
          do v1 = 1, nv1
            tmp(o1, :) = csc(no + v1, 1:no)
            do o2 = 1, no
              tmp2 = tmp
              do v2 = 1, nv2
                tmp2(:, o2) = csc(1:no, no + v2)
                tmp2(o1, o2) = csc(no + v1, no + v2)
                cdet = matdet(tmp2)
                do st1 = 1, ns1
                  do st2 = 1, ns2
                    ss(st1, st2) = ss(st1, st2) + cdet * wf1((o1-1)*nv1 + v1, st1) &
                                                    &  * wf2((o2-1)*nv2 + v2, st2)
                  end do
                end do
              end do
            end do
          end do
        end do
        !$omp end do
        !$omp end parallel
    end subroutine ssblock


end module cisoverlapmod
