!----------------------------------------------------------------------------------------------
! MODULE: SparseWFMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date November, 2017
!----------------------------------------------------------------------------------------------
module sparsewfmod
    use fortutils
    use sparsemod
    implicit none

    private
    public :: gensparsewf

contains

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: GenSparseWF
    !
    ! DESCRIPTION:
    !> @brief Truncate a set of wave functions.
    !> @details
    !! For each wave function a sparse vector is created until the norm of the vector is greater 
    !! than the threshold.
    !----------------------------------------------------------------------------------------------
    subroutine gensparsewf(rhf, nst, ndet, wf, thr, sprs)
        integer, intent(in) :: rhf !< Restricted/unrestricted.
        integer, intent(in) :: nst !< Number of states.
        integer, intent(in) :: ndet(:) !< Number of determinants in wave function.
        type(rmat) :: wf(:) !< Wave function coefficients for each spin.
                            !! Dimensions of wf(s)%c = ndet(s) x nexst
        real(dp), intent(in) :: thr !< Threshold for including coefficients.
        type(sparsev), allocatable :: sprs(:, :) !< Truncated wave functions.
                                                 !! Dimensions: rhf x nst
        real(dp) :: tabswf(sum(ndet)) !< Absolute values of coefficients of all spins.
        logical :: tcmask(sum(ndet)) !< Mask for elements still not added to sparse vector.
        type(ivec) :: tindex(rhf) !< Temporary vector of indexes for each spin.
        integer :: p(1) !< Position of current largest coefficient.
        integer :: i, st, s
        real(dp) :: norm
        
        if (.not. allocated(sprs)) allocate(sprs(rhf, nst))
        do s = 1, rhf
            allocate(tindex(s)%c(ndet(s)))
        end do
      
        do st = 1, nst
            tcmask = .true.
            tabswf(1:ndet(1)) = abs(wf(1)%c(:, st))
            if (rhf == 2) tabswf(ndet(1) + 1 : sum(ndet)) = abs(wf(2)%c(:, st))

            sprs(:, st)%n = 0
            norm = 0.0_dp 
            do i = 1, sum(ndet)
                p = maxloc(tabswf, tcmask) ! Find largest element.
                if (p(1) <= ndet(1)) then ! Check if it is in alpha or beta part of vector.
                    sprs(1, st)%n = sprs(1, st)%n + 1
                    tindex(1)%c(sprs(1, st)%n) = p(1)
                else
                    sprs(2, st)%n = sprs(2, st)%n + 1
                    tindex(2)%c(sprs(2, st)%n) = p(1) - ndet(1)
                end if
                norm = norm + tabswf(p(1))**2 ! Check norm.
                if (norm >= thr) exit
                tcmask(p(1)) = .false. ! Skip coefficient in next loop.
            end do

            !< @todo sort?
            do s = 1, rhf
                if (allocated(sprs(s, st)%i)) deallocate(sprs(s, st)%i)
                if (allocated(sprs(s, st)%v)) deallocate(sprs(s, st)%v)
                allocate(sprs(s, st)%i(sprs(s, st)%n))
                allocate(sprs(s, st)%v(sprs(s, st)%n))
                sprs(s, st)%i = tindex(s)%c(1:sprs(s, st)%n)
                sprs(s, st)%v = wf(s)%c(sprs(s, st)%i, st)
            end do
        end do
    end subroutine gensparsewf

    
end module sparsewfmod
