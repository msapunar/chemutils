module qminfotype
    use fortutils
    use atombasistype, only : atombasis
    use ccgmod, only : ccg

    type qminfo
        integer :: rhf = 0 !< Restricted/unrestricted calculation.
        character(len=:), allocatable :: symmetry !< Symmetry of the molecule.

        ! Atoms.
        integer :: nat = 0 !< Number of atoms.
        character(len=2), allocatable :: asym(:) !< Atom symbols.
        real(dp), allocatable :: axyz(:, :) !< Coordinates of atoms.
                                            !! Dimensions: 3 x nat
        type(atombasis), allocatable :: abas(:) !< Basis set for each atom.
                                                !! Dimensions: nat

        ! Atomic orbitals.
        integer :: naos = 0 !< Number of atomic orbitals (spherical basis).
        integer :: naoc = 0 !< Number of atomic orbitals (cartesian basis).
        type(ccg), allocatable :: ao_c(:) !< Basis set of contracted cartesian Gaussian functions.
                                          !! Dimensions: naoc
        real(dp), allocatable :: ao_c2s(:, :) !< Transformation matrix between ao basis types.

        ! Molecular orbitals
        integer :: nmo = 0 !< Number of molecular orbitals.
        real(dp), allocatable :: mo_en(:, :) !< Energies of the molecular orbitals.
                                         !! Dimensions: nmo x rhf
        real(dp), allocatable :: mo_c(:, :, :) !< Coefficients of the molecular orbitals.
                                         !! Dimensions: nao x nmo x rhf
        real(dp), allocatable :: mo_o(:, :) !< Occupation numbers.
                                         !! Dimensions: nmo x rhf
        logical, allocatable :: mo_omask(:, :) !< Occupied orbitals mask.
                                            !! Dimensions: nmo x 2
        logical, allocatable :: mo_amask(:) !< Active orbitals mask.
                                         !! Dimensions: nmo x rhf
        integer :: nmo_o(2) = 0 !< Number of occupied mos.
        integer :: nmo_v(2) = 0 !< Number of virtual mos.
        integer :: nmo_a = 0 !< Number of active mos.
        integer :: nmo_f = 0 !< Number of frozen mos.
        integer :: nmo_ao(2) = 0 !< Number of active occupied mos.
        integer :: nmo_av(2) = 0 !< Number of active virtual mos.
        integer :: nmo_fo(2) = 0 !< Number of frozen occupied mos.
        integer :: nmo_fv(2) = 0 !< Number of frozen virtual mos.

        ! Electronic states.
        integer :: nst = 1 !< Number of electronic states.
        real(dp), allocatable :: en(:) !< Total QM state energies.
                                       !! Dimensions: nst
        integer :: cst = 0 !< Selected electronic state.
        real(dp), allocatable :: agrd(:, :) !< Gradient of atoms in electronic state cst.
                                            !! Dimensions: 3 x nat
        ! Excited states.
        integer :: nexst = 0 !< Number of excited states.
        integer :: ndet(2) = 0 !< Dimension of wave functions.
        type(rmat) :: wf(2) !< Wave function coefficients for each spin.
                            !! Dimensions of wf(s)%c = ndet(s) x nexst
        real(dp), allocatable :: exen(:) !< Excitation energies.
                                         !! Dimensions: nexst
        real(dp), allocatable :: exos(:) !< Oscillator strengths.
                                         !! Dimensions: nexst
    contains
        procedure :: countnmo
    end type qminfo

contains

    subroutine countnmo(self)
        class(qminfo) :: self
        if (.not. allocated(self%mo_omask)) then
            write(stderr, *) 'Error in CountMO subroutine. Occupied orbitals mask not allocated.'
            stop
        end if
        if (.not. allocated(self%mo_amask)) then
            write(stderr, *) 'Warning in CountMO subroutine.'
            write(stderr, *) 'Active orbitals mask not allocated, assuming all orbitals active.'
            allocate(self%mo_amask(size(self%mo_omask, 1)))
            self%mo_amask = .true.
        end if
        if (self%nmo == 0) self%nmo=size(self%mo_amask)
        if ((self%nmo /= size(self%mo_amask)) .or. (self%nmo /= size(self%mo_omask, 1))) then
            write(stderr, *) 'Error in CountMO subroutine. Array size mismatch.'
            stop
        end if
        self%nmo_a = count(self%mo_amask)
        self%nmo_f = self%nmo - self%nmo_a
        self%nmo_o = count(self%mo_omask, 1)
        self%nmo_v = count(.not. self%mo_omask, 1)
        self%nmo_ao(1) = count(self%mo_omask(:, 1) .and. self%mo_amask)
        self%nmo_ao(2) = count(self%mo_omask(:, 2) .and. self%mo_amask)
        self%nmo_av(1) = self%nmo_a - self%nmo_ao(1)
        self%nmo_av(2) = self%nmo_a - self%nmo_ao(2)
        self%nmo_fo = self%nmo_o - self%nmo_ao
        self%nmo_fv = self%nmo_o - self%nmo_av
    end subroutine countnmo

end module qminfotype
