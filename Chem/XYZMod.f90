!----------------------------------------------------------------------------------------------
! MODULE: XYZMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date May, 2017
!
! DESCRIPTION:
!> @brief Contains subroutines for reading and writing XYZ files.
!----------------------------------------------------------------------------------------------
module xyzmod
    ! Import variables
    use fortutils
    ! Import classes
    use stringmod
    implicit none

    private
    public :: xyzread
    public :: xyzwrite

contains


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: XYZRead
    !> @brief Read contents of an XYZ file into arrays.
    !> @details
    !! Can read number of atoms, list of atoms and coordinates of atoms.
    !> @note Converts gemetry values from Angstrom to atomic units.
    !----------------------------------------------------------------------------------------------
    subroutine xyzread(fname, natom, atom, geom)
        use constants
        character(len=*), intent(in) :: fname !< Name of XYZ file.
        integer, intent(out), optional :: natom !< Number of atoms.
        character(len=2), allocatable, intent(out), optional :: atom(:) !< List of atoms.
        real(dp), allocatable, intent(out), optional :: geom(:, :) !< Geometry.
        integer :: i
        integer :: tnatom
        integer :: inunit
        logical :: check

        inquire(file=fname, exist=check)
        if (.not. check) then
            write(stderr,*) 'Error in XYZMod module, XYZRead subroutine.'
            write(stderr,*) ' File (', fname,') not found.'
            stop
        end if

        open(newunit=inunit, file=fname, action='read')
        read(inunit, *) tnatom
        if (present(natom)) natom = tnatom
        if (present(geom)) then
            if (allocated(geom)) then
                if (size(geom, 2) /= tnatom) then
                    write(stderr,*) 'Error in XYZMod module, XYZRead subroutine.'
                    write(stderr,'(3x,a,i0)') ' Geometry array shape: ', shape(geom)
                    write(stderr,'(3x,a,i0)') ' XYZ natom: ', tnatom
                    stop
                end if
            else
                allocate(geom(3, tnatom))
            end if
        end if
        if (present(atom)) then
            if (allocated(atom)) then
                if (size(atom, 1) /= tnatom) then
                    write(stderr,*) 'Error in XYZMod module, XYZReadArray subroutine.'
                    write(stderr,'(3x,a,i0)') ' Atom list size: ', shape(atom)
                    write(stderr,'(3x,a,i0)') ' XYZ natom: ', tnatom
                    stop
                end if
            else
                allocate(atom(tnatom))
            end if
        end if
        read(inunit, *)
        do i = 1, tnatom
            call line%readline(inunit)
            call line%parse(' ')
            if (present(atom)) read(line%args(1), *) atom(i)
            if (present(geom)) then
                read(line%args(2), *) geom(1, i)
                read(line%args(3), *) geom(2, i)
                read(line%args(4), *) geom(3, i)
            end if
        end do
        close(inunit)
        if (present(geom)) geom = geom / a0_A
    end subroutine xyzread


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: XYZWrite
    !
    ! DESCRIPTION:
    !> @brief Write XYZ file from atom list and geometry.
    !> @note Converts gemetry values from atomic units to Angstrom.
    !----------------------------------------------------------------------------------------------
    subroutine xyzwrite(fname, atom, geom, msg, append)
        use constants
        character(len=*), intent(in) :: fname !< Name of XYZ file.
        character(len=2), intent(in) :: atom(:) !< List of atoms.
        real(dp), intent(in) :: geom(:, :) !< Geometry.
        character(len=*), intent(in), optional :: msg !< Optional title line.
        logical, intent(in), optional :: append
        logical :: new
        logical :: check
        integer :: outunit
        integer :: i
        character(len=:), allocatable :: title

        title = ' '
        if (present(msg)) title = msg
        new = .true.
        if (present(append)) then
            if (append) new = .false.
        end if

        inquire(file=fname, exist=check)
        if (check .and. (.not. new)) then
            open(newunit=outunit, file=fname, status='old', position='append', action='write')
        else
            open(newunit=outunit, file=fname, action='write')
        end if

        open(newunit=outunit, file=fname, action='write')
        write(outunit,'(1x,i0)') size(atom)
        write(outunit, '(a)') title
        do i = 1, size(atom)
            write(outunit, '(1x,a,4x,3(f20.14,2x))') atom(i), geom(:, i) * a0_A
        end do
        close(outunit)
    end subroutine xyzwrite


end module xyzmod
