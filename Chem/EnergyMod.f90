!--------------------------------------------------------------------------------------------------
! MODULE: EnergyMod
!
! DESCRIPTION: 
!> @brief Contains subroutines for calculating kinetic/potential energies.
!--------------------------------------------------------------------------------------------------
module energymod
    ! Import variables
    use fortutils
    implicit none

    private
    public :: ekin
    public :: eelp

contains

    !----------------------------------------------------------------------------------------------
    ! FUNCTION: EKin
    !
    ! DESCRIPTION:
    !> @brief Calculate the kinetic energy for a set of particles.
    !----------------------------------------------------------------------------------------------
    pure function ekin(mass, velo) result (en)
        real(dp), intent(in) :: mass(:)
        real(dp), intent(in) :: velo(:, :)
        real(dp) :: en
        integer :: i

        en = 0.0_dp
        do i = 1, size(mass)
            en = en + mass(i) * dot_product(velo(:, i), velo(:, i)) * 0.5_dp
        end do
    end function ekin

    !----------------------------------------------------------------------------------------------
    ! FUNCTION: EElP
    !
    ! DESCRIPTION:
    !> @brief Calculate the electrostatic potential energy between two sets of point charges.
    !> @details
    !! Optionally, a cutoff distance can be passed to the function. If present, interactions 
    !! between pairs of charges whose distance is greater than the cutoff distance will not be 
    !! added to the final energy.
    !----------------------------------------------------------------------------------------------
    pure function eelp(geom1, geom2, q1, q2, cutoff) result (en)
        real(dp), intent(in) :: geom1(:, :)
        real(dp), intent(in) :: geom2(:, :)
        real(dp), intent(in) :: q1(:)
        real(dp), intent(in) :: q2(:)
        real(dp), optional, intent(in) :: cutoff
        real(dp) :: en
        integer :: i
        integer :: j
        real(dp) :: dxyz(size(geom1, 1))
        real(dp) :: d

        en = 0.0_dp
        do i = 1, size(q1)
            do j = 1, size(q2)
                dxyz = geom1(:, i) - geom2(:, j)
                d = sqrt(dot_product(dxyz, dxyz))
                if (present(cutoff)) then
                    if (d > cutoff) cycle
                end if
                en = en + q1(i) * q2(j) / d
            end do
        end do
    end function eelp


end module energymod
