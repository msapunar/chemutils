module coordinatesmod
    use fortutils
    implicit none

    private
    public :: cart2polr


contains

    
    !----------------------------------------------------------------------------------------------
    ! Function: Cart2Polr
    !> @brief Convert vector from cartesian to polar coordinates.
    !----------------------------------------------------------------------------------------------
    pure function cart2polr(c) result(p)
        real(dp), intent(in) :: c(3)
        real(dp) :: p(3)
        real(dp) :: rp
        real(dp), parameter :: pig2 = 6.28318530717959_dp

        p(1) = sqrt(c(1)*c(1) + c(2)*c(2) + c(3)*c(3))
        rp = sqrt(c(1)*c(1) + c(2)*c(2))
        if (abs(p(1)) < tinydp) then
            p(2) = 0.0_dp
            p(3) = 0.0_dp
        else
            p(2) = acos(c(3) / p(1))
            if (abs(rp) < tinydp) then
                p(3) = 0.0_dp
            else
                p(3) = atan2(c(2), c(1))
            end if
            if (c(2) < 0) p(3) = p(3) + pig2
        end if

    end function cart2polr


end module coordinatesmod
