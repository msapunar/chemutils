!--------------------------------------------------------------------------------------------------
! MODULE: MOType
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date November, 2016
!
! DESCRIPTION: 
!> @brief Contains the MolOrbs type and accompanying subroutines.
!--------------------------------------------------------------------------------------------------
module motype
    ! Import variables
    use fortutils
    implicit none

    private
    public :: molorbs


    !----------------------------------------------------------------------------------------------
    ! TYPE: MolOrbs
    !
    ! DESCRIPTION:
    !> @brief Contains information about the molecular orbitals of the system.
    !----------------------------------------------------------------------------------------------
    type molorbs
        integer :: rhf = 0 !< Restricted/unrestricted orbitals.
        integer :: n = 0 !< Number of molecular orbitals.
        integer :: nbas = 0 !< Number of basis functions.
        real(dp), allocatable :: c(:, :, :) !< Coefficients of the molecular orbitals.
                                            !! Dimensions: nbas x n x rhf
        real(dp), allocatable :: e(:, :) !< Energies of the molecular orbitals.
                                         !! Dimensions: n x rhf
        real(dp), allocatable :: o(:, :) !< Occupation numbers.
                                         !! Dimensions: n x rhf
        logical, allocatable :: amask(:, :) !< Active orbitals mask.
                                            !! Dimensions: n x rhf
        logical, allocatable :: omask(:, :) !< Occupied orbitals mask.
                                            !! Dimensions: n x rhf
    contains
        procedure :: no
        procedure :: nv
        procedure :: na
        procedure :: nf
        procedure :: nao
        procedure :: nav
        procedure :: nfo
        procedure :: nfv
        procedure :: chbasis
    end type molorbs
 
contains

    !----------------------------------------------------------------------------------------------
    ! FUNCTION: no
    !> @brief Return the number of occupied orbitals of spin 'i'.
    !----------------------------------------------------------------------------------------------
    function no(self, i) result(r)
        class(molorbs), intent(in) :: self
        integer, intent(in) :: i !< Spin.
        integer :: r !< Number of occupied orbitals.
        integer :: n(self%rhf)
        n = count(self%omask, 1)
        r = n(i)
    end function no


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: nv
    !> @brief Return the number of virtual (unoccupied) orbitals of spin 'i'.
    !----------------------------------------------------------------------------------------------
    function nv(self, i) result(r)
        class(molorbs), intent(in) :: self
        integer, intent(in) :: i !< Spin.
        integer :: r !< Number of virtual orbitals.
        integer :: n(self%rhf)
        n = count(.not. self%omask, 1)
        r = n(i)
    end function nv


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: na
    !> @brief Return the number of active (not frozen) orbitals of spin 'i'.
    !----------------------------------------------------------------------------------------------
    function na(self, i) result(r)
        class(molorbs), intent(in) :: self
        integer, intent(in) :: i !< Spin.
        integer :: r !< Number of virtual orbitals.
        integer :: n(self%rhf)
        if (.not. allocated(self%amask)) then
            r = self%n
        else
            n = count(self%amask, 1)
            r = n(i)
        end if
    end function na


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: nf
    !> @brief Return the number of frozen orbitals of spin 'i'.
    !----------------------------------------------------------------------------------------------
    function nf(self, i) result(r)
        class(molorbs), intent(in) :: self
        integer, intent(in) :: i !< Spin.
        integer :: r !< Number of virtual orbitals.
        integer :: n(self%rhf)
        if (.not. allocated(self%amask)) then
            r = 0
        else
            n = count(.not. self%amask, 1)
            r = n(i)
        end if
    end function nf


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: nao
    !> @brief Return the number of active occupied orbitals of spin 'i'.
    !----------------------------------------------------------------------------------------------
    function nao(self, i) result(r)
        class(molorbs), intent(in) :: self
        integer, intent(in) :: i
        integer :: n(self%rhf)
        integer :: r
        if (.not. allocated(self%amask)) then
            r = self%no(i)
        else
            n = count(self%omask .and. self%amask, 1)
            r = n(i)
        end if
    end function nao


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: nav
    !> @brief Return the number of active virtual (unoccupied) orbitals of spin 'i'.
    !----------------------------------------------------------------------------------------------
    function nav(self, i) result(r)
        class(molorbs), intent(in) :: self
        integer, intent(in) :: i
        integer :: n(self%rhf)
        integer :: r
        if (.not. allocated(self%amask)) then
            r = self%nv(i)
        else
            n = count((.not. self%omask) .and. self%amask, 1)
            r = n(i)
        end if
    end function nav


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: nfo
    !> @brief Return the number of frozen occupied orbitals of spin 'i'.
    !----------------------------------------------------------------------------------------------
    function nfo(self, i) result(r)
        class(molorbs), intent(in) :: self
        integer, intent(in) :: i
        integer :: n(self%rhf)
        integer :: r
        if (.not. allocated(self%amask)) then
            r = 0
        else
            n = count(self%omask .and. (.not. self%amask), 1)
            r = n(i)
        end if
    end function nfo


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: nfv
    !> @brief Return the number of frozen virtual orbitals of spin 'i'.
    !----------------------------------------------------------------------------------------------
    function nfv(self, i) result(r)
        class(molorbs), intent(in) :: self
        integer, intent(in) :: i
        integer :: n(self%rhf)
        integer :: r
        if (.not. allocated(self%amask)) then
            r = 0
        else
            n = count(.not. (self%omask .and. self%amask), 1)
            r = n(i)
        end if
    end function nfv


    subroutine chbasis(self, nbas, chmat)
        class(molorbs), intent(inout) :: self !< Set of molecular orbitals.
        integer, intent(in) :: nbas !< Number of basis functions in new basis set.
        real(dp), intent(in) :: chmat(nbas, self%nbas) !< Transformation matrix.

        integer :: s !< Spin iterator.
        real(dp) :: tc(nbas, self%n, self%rhf) !< Temporary coefficients matrix.

        do s = 1, self%rhf
            tc(:, :, s) = matmul(chmat, self%c(:, :, s))
        end do
        deallocate(self%c)
        self%nbas = nbas
        allocate(self%c(self%nbas, self%n, self%rhf))
        self%c = tc
    end subroutine chbasis

end module motype
