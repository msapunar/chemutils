!--------------------------------------------------------------------------------------------------
! MODULE: cCGMod
!
! DESCRIPTION: 
!> @brief Contains the cCG type and accompanying subroutines.
!--------------------------------------------------------------------------------------------------
module ccgmod
    ! Import variables
    use fortutils
    implicit none

    private
    public :: ccg
    public :: genccgs

    real(dp), parameter :: pito3over2=5.5683279968317078_dp

    !----------------------------------------------------------------------------------------------
    ! TYPE: cCG
    !
    ! DESCRIPTION:
    !> @brief Contains a single contracted Cartesian Gaussian function.
    !----------------------------------------------------------------------------------------------
    type ccg
        integer :: ang(3) !< Angular momenta.
        integer :: npr !< Number of primitive functions.
        real(dp) :: q0(3) !< Coordinates of the center of the Gaussian function.
        real(dp), allocatable :: z(:) !< Zeta coefficients (exponent factors).
        real(dp), allocatable :: b(:) !< Beta coefficients (linear factors).
    contains
        procedure :: initnorm
        procedure :: val
        procedure :: grad
    end type ccg


contains


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: InitNorm
    !
    ! DESCRIPTION:
    !> @brief 
    !! Multiply contraction coefficients by the angular momentum dependent part.
    !> @details
    !! This subroutine does not compute the norm of the contracted cartesian gaussian function,
    !! just multiplies by the norm:
    !! 
    !! b'(i) = b(i) * 2^(3/4 + l/2) / Sqrt(Y((lx+1)/2)*Y((ly+1)/2)*Y((lz+1)/2)) * z(i)^(3/4 + l/2)
    !!
    !----------------------------------------------------------------------------------------------
    subroutine initnorm(self)
        class(ccg) :: self !< Function.
        integer :: i
        integer :: lx
        integer :: ly
        integer :: lz
        real(dp) :: pow
        real(dp) :: const
        
        lx = self%ang(1)
        ly = self%ang(2)
        lz = self%ang(3)
        pow = 0.75_dp + (lx + ly + lz) * 0.5_dp
        const = 2**pow / sqrt(triplegammahalfint(2*lx, 2*ly, 2*lz))
        do i = 1, self%npr
           self%b(i) = self%b(i) * const * self%z(i)**pow
        end do
    end subroutine initnorm


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Val
    !
    ! DESCRIPTION:
    !> @brief 
    !! Calculate the value of the function at a specific point.
    !----------------------------------------------------------------------------------------------
    function val(self, xyz) result (rval)
        class(ccg), intent(in) :: self !< Function.
        real(dp), intent(in) :: xyz(3) !< Coordinates of the point.
        real(dp) :: rval !< Value of the function.
        integer :: i
        integer :: l(3)
        real(dp) :: dxyz(3)
        real(dp) :: dr2
        real(dp) :: ang
        real(dp) :: bexpsum
        
        dxyz = xyz - self%q0
        dr2 = sum(dxyz**2)
        
        l = self%ang
        ang = 1.0_dp
        if (l(1) >= 1) ang = ang * dxyz(1)**l(1)
        if (l(2) >= 1) ang = ang * dxyz(2)**l(2)
        if (l(3) >= 1) ang = ang * dxyz(3)**l(3)
        
        bexpsum = 0.0_dp
        do i = 1, self%npr
            bexpsum = bexpsum + self%b(i) * exp(-self%z(i) * dr2)
        end do
        rval = ang * bexpsum
    end function val


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Grad
    !
    ! DESCRIPTION:
    !> @brief 
    !! Calculate the gradient of the function at a specific point.
    !----------------------------------------------------------------------------------------------
    function grad(self, xyz) result (rgrad)
        class(ccg), intent(in) :: self
        real(dp), intent(in) :: xyz(3)
        real(dp) :: rgrad(3)
        
        integer :: i
        integer :: l(3)
        real(dp) :: dq(3)
        real(dp) :: dr2
        real(dp) :: ang
        real(dp) :: bexp
        real(dp) :: bexpsum
        real(dp) :: zbexpsum
        
        dq = xyz - self%q0
        dr2 = sum(dq**2)
        
        l = self%ang
        ang = 1.0_dp
        if (l(1) >= 1) ang = ang * dq(1)**l(1)
        if (l(2) >= 1) ang = ang * dq(2)**l(2)
        if (l(3) >= 1) ang = ang * dq(3)**l(3)
        
        bexpsum = 0.0_dp
        zbexpsum = 0.0_dp
        do i = 1, self%npr
            bexp = self%b(i) * exp(-self%z(i) * dr2)
            bexpsum = bexpsum + bexp
            zbexpsum = zbexpsum + bexp * self%z(i)
        end do
        rgrad = - 2.0_dp * ang * dq * zbexpsum
        
        ang = 1.0_dp
        if (l(1) >= 1) then
            if (l(1) /= 1) ang = ang * dq(1)**(l(1)-1)
            if (l(2) >= 1) ang = ang * dq(2)**(l(2))
            if (l(3) >= 1) ang = ang * dq(3)**(l(3))
            rgrad(1) = rgrad(1) + l(1) * ang * bexpsum
        end if
        ang = 1.0_dp
        if (l(2) >= 1) then
            if (l(1) >= 1) ang = ang * dq(1)**(l(1))
            if (l(2) /= 1) ang = ang * dq(2)**(l(2)-1)
            if (l(3) >= 1) ang = ang * dq(3)**(l(3))
            rgrad(2) = rgrad(2) + l(2) * ang * bexpsum
        end if
        ang = 1.0_dp
        if (l(3) >= 1) then
           if (l(1) >= 1) ang = ang * dq(1)**(l(1))
           if (l(2) >= 1) ang = ang * dq(2)**(l(2))
           if (l(3) /= 1) ang = ang * dq(3)**(l(3)-1)
           rgrad(3) = rgrad(3) + l(3) * ang * bexpsum
        end if
        
    end function grad


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: TripleGammaHalfInt
    !
    ! DESCRIPTION:
    !> @brief 
    !! Return the part of a Cartesian Gaussian integral that depends only on the angular momenta.
    !> @details
    !!
    !! ggg = Gamma((lx+1)/2)*Gamma((ly+1)/2)*Gamma((lz+1)/2)
    !!     = (lx-1)!!(ly-1)!!(lz-1)!! * Pi^(3/2) / 2^((lx+ly+lz)/2)
    !! 
    !----------------------------------------------------------------------------------------------
    function triplegammahalfint(lx,ly,lz) result(ggg)
      integer, intent(in) :: lx !< Angular momentum along x axis.
      integer, intent(in) :: ly !< Angular momentum along y axis.
      integer, intent(in) :: lz !< Angular momentum along z axis.
      real(dp) :: ggg !< Result.
      integer, parameter, dimension(-1:12) :: dfact=[1,1,1,2,3,8,15,48,105,384,945,3840,10395,46080]

      if (mod(lx+ly+lz,2) /= 0) stop 'Error. Odd number passed to TripleGammaHalfInt'
      ggg = dfact(lx-1) * dfact(ly-1) * dfact(lz-1) / 2.0_dp**((lx+ly+lz)/2) * pito3over2

    end function triplegammahalfint



    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: GencCGs
    !
    ! DESCRIPTION:
    !> @brief Defines a normalized set of contracted cartesian gaussian functions.
    !> @details
    !! The basis set for each atom is read from the abas variable and the centers of the functions
    !! are pointed at the axyz array. The basis functions are normalized and expanded into
    !! contracted Cartesian Gaussian functions with appropriate angular momenta and the new
    !! functions are normalized.
    !! The subroutine also generates an array for transforming between the cartesian and spherical
    !! representations of the basis set.
    !----------------------------------------------------------------------------------------------
    subroutine genccgs(axyz, abas, nsphe, ncart, ccgs, ao_c2s)
        use angmomarrays
        use atombasistype
        real(dp), intent(in) :: axyz(:, :)
        type(atombasis), intent(inout) :: abas(:)
        integer, intent(out) :: nsphe
        integer, intent(out) :: ncart
        type(ccg), allocatable, intent(out) :: ccgs(:)
        real(dp), allocatable, intent(out) :: ao_c2s(:, :)
      
        integer :: c
        integer :: s
        integer :: i
        integer :: j
        integer :: k
        integer :: kmax
        integer :: angmom(1:3,1:21)
        
        ncart = 0
        nsphe = 0
        do i = 1, size(abas)
            if ((abas(i)%ncart == 0) .or. (abas(i)%nsphe == 0)) then
                write(stderr,*) 'Error in BasisSet module, GenerateCart subroutine.'
                write(stderr,'(a,i0,a)') ' Number of basis functions not defined for atom ', i, '.'
                stop
            end if
            call abas(i)%normalize()
            ncart = ncart + abas(i)%ncart
            nsphe = nsphe + abas(i)%nsphe
        end do

        if (.not. allocated(ccgs)) allocate(ccgs(ncart))
        if (.not. allocated(ao_c2s)) allocate(ao_c2s(nsphe, ncart))
        ao_c2s = 0.0_dp
        c = 1
        s = 1
        
        do i = 1, size(abas)
            do j = 1, abas(i)%nfunc
                angmom = 0
                select case (abas(i)%typ(j))
                    case('s')
                        kmax = 1
                        ao_c2s(s, c) = 1
                        s = s + 1
                    case('p')
                        kmax = 3
                        angmom(1:3, 1:3) = pangmom
                        ao_c2s(s:s+2, c:c+2) = ptransform
                        s = s + 3
                    case('d')
                        kmax = 6
                        angmom(1:3, 1:6) = dangmom
                        ao_c2s(s:s+4, c:c+5) = dtransform
                        s = s + 5
                    case('f')
                        kmax = 10
                        angmom(1:3,1:10) = fangmom
                        ao_c2s(s:s+6, c:c+9) = ftransform
                        s = s + 7
                    case('g')
                        kmax = 15
                        angmom(1:3, 1:15) = gangmom
                        ao_c2s(s:s+8, c:c+14) = gtransform
                        s = s + 9
                    case('h')
                        kmax = 21
                        angmom(1:3, 1:21) = hangmom
                        ao_c2s(s:s+10, c:c+20) = htransform
                        s = s + 11
                end select
                do k = 1, kmax
                   ccgs(c)%ang = angmom(1:3,k)
                   ccgs(c)%npr = abas(i)%npr(j)
                   if (.not. allocated(ccgs(c)%z)) allocate(ccgs(c)%z(abas(i)%npr(j)))
                   if (.not. allocated(ccgs(c)%b)) allocate(ccgs(c)%b(abas(i)%npr(j)))
                   ccgs(c)%z = abas(i)%z(j)%c
                   ccgs(c)%b = abas(i)%b(j)%c
                   ccgs(c)%q0 = axyz(:, i)
                   call ccgs(c)%initnorm
                   c = c + 1
                end do
            end do
        end do
    
        if ((c-1) /= ncart) then
            write(stderr,*) 'Error in BasisSet module, GenerateCart subroutine.'
            write(stderr,*) ' NCart mismatch.'
            stop
        end if
        if ((s-1) /= nsphe) then
            write(stderr,*) 'Error in BasisSet module, GenerateCart subroutine.'
            write(stderr,*) ' NSphe mismatch.'
            stop
        end if
    end subroutine genccgs


end module ccgmod
