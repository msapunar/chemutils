!----------------------------------------------------------------------------------------------
! MODULE: MoldenWriteMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date June, 2017
!
! DESCRIPTION:
!> @brief Contains subroutines for writing a Molden format file.
!> @details
!! The Molden file format is organized into sections marked by keywords (ex. [Atoms]). Each 
!! subroutine in this module writes a section of the file. 
!----------------------------------------------------------------------------------------------
module moldenwritemod
    ! Import variables
    use constants
    implicit none

    private

    public :: moldenwriteatoms
    public :: moldenwritegto
    public :: moldenwritesinglemo
    public :: moldenwritemo
    public :: moldenwritefull


contains


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: MoldenWriteAtoms
    !
    ! DESCRIPTION:
    !> @brief Write the molden [Atoms] section.
    !----------------------------------------------------------------------------------------------
    subroutine moldenwriteatoms(outunit, sym, geom)
        use elementsmod
        integer, intent(in) :: outunit !< Unit in which the file is open.
        character(len=2), intent(in) :: sym(:)
        real(dp), intent(in) :: geom(:, :)
        character(len=2), parameter :: lenunit = 'AU'
        integer :: i

        write(outunit, '(a)') '[Atoms] '//lenunit
        do i = 1, size(geom, 2)
            write(outunit, 1000) sym(i), i, element_s2z(sym(i)), geom(:, i)
        end do

        1000 format (1x, a2, 2(1x,i4), 3(1x,e20.14))
    end subroutine moldenwriteatoms
 

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: MoldenWriteGTO
    !
    ! DESCRIPTION:
    !> @brief Write the molden [GTO] section.
    !> @details
    !! Write the full basis set of each atom from the atom%bas variable to the file.
    !----------------------------------------------------------------------------------------------
    subroutine moldenwritegto(outunit, abas)
        use atombasistype
        integer, intent(in) :: outunit !< Unit in which the file is open.
        type(atombasis), intent(in) :: abas(:) !< Atomic basis sets.
        integer :: i
        integer :: j
        integer :: k
       
        write(outunit, '(a)') '[GTO]'
        do i = 1, size(abas)
            write(outunit, 2000) i, 0
            do j = 1, abas(i)%nfunc
                write(outunit, 2001) abas(i)%typ(j), abas(i)%npr(j), 1.00_dp
                do k = 1, abas(i)%npr(j)
                    write(outunit, 2002) abas(i)%z(j)%c(k), abas(i)%b(j)%c(k)
                end do
            end do
            write(outunit, '(a)') ' '
        end do
        

        2000 format (1x, i4, 1x, i1)
        2001 format (1x, a, 3x, i2, 1x, f4.2)
        2002 format (2(1x, e20.14))
    end subroutine moldenwritegto


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: MoldenWriteSingleMO
    !
    ! DESCRIPTION:
    !> @brief Write a single molecular orbital to the [MO] section.
    !----------------------------------------------------------------------------------------------
    subroutine moldenwritesinglemo(outunit, i, sym, s, e, o, c)
        integer, intent(in) :: outunit !< Unit in which the file is open.
        integer, intent(in) :: i !< Number of the MO.
        character(len=*), intent(in) :: sym !< Symmetry group.
        integer, intent(in) :: s !< Spin.
        real(dp), intent(in) :: e !< Energy.
        real(dp), intent(in) :: o !< Occupation number.
        real(dp), intent(in) :: c(:) !< Coefficients.
        integer :: j

        write(outunit, 3000) i, sym
        write(outunit, 3001) e
        if (s == 1) write(outunit, 3002) 'Alpha'
        if (s == 2) write(outunit, 3002) 'Beta'
        write(outunit, 3003) o
        do j = 1, size(c)
            write(outunit, 3004) j, c(j)
        end do

        3000 format (1x, 'Sym=', i6, a)
        3001 format (1x, 'Ene=', 1x, e20.14)
        3002 format (1x, 'Spin=', 1x, a)
        3003 format (1x, 'Occup=', 1x, f8.6)
        3004 format (i6, 1x, e20.14)
    end subroutine moldenwritesinglemo


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: MoldenWriteMO
    !
    ! DESCRIPTION:
    !> @brief Write the molden [MO] section.
    !> @details
    !! Write the full set of molecular orbitals to a molden file.
    !----------------------------------------------------------------------------------------------
    subroutine moldenwritemo(outunit, mo_o, mo_e, mo_c)
        integer, intent(in) :: outunit !< Unit in which the file is open.
        real(dp), intent(in) :: mo_o(:, :) !< MO occupation numbers.
        real(dp), intent(in) :: mo_e(:, :) !< MO energies.
        real(dp), intent(in) :: mo_c(:, :, :) !< MO coefficients.
        integer :: i
        integer :: s

        write(outunit, '(a)') '[MO]'
        do i = 1, size(mo_e, 1)
            do s = 1, size(mo_e, 2)
                call moldenwritesinglemo(outunit, i, 'a   ', s, &
                                       & mo_e(i, s), mo_o(i, s), mo_c(:, i, s))
            end do
        end do
    end subroutine moldenwritemo


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: MoldenWriteFull
    !
    ! DESCRIPTION:
    !> @brief Write all information to a molden file.
    !> @details
    !! This subroutine only calls subroutines which write parts of the file. 
    !----------------------------------------------------------------------------------------------
    subroutine moldenwritefull(outfile, sym, geom, abas, mo_o, mo_e, mo_c)
        use atombasistype
        character(len=*), intent(in) :: outfile !< Output file.
        character(len=2), intent(in) :: sym(:) !< List of atoms.
        real(dp), intent(in) :: geom(:, :) !< Coordinates.
        type(atombasis), intent(in) :: abas(:) !< Atomic basis sets.
        real(dp), intent(in) :: mo_o(:, :) !< MO occupation numbers.
        real(dp), intent(in) :: mo_e(:, :) !< MO energies.
        real(dp), intent(in) :: mo_c(:, :, :) !< MO coefficients.
        integer :: outunit

        open(newunit=outunit, file=outfile, action='write')
        write(outunit, '(a)') '[Molden Format]'
        write(outunit, '(a)') '[Title]'
        write(outunit, '(a)')
        call moldenwriteatoms(outunit, sym, geom)
        call moldenwritegto(outunit, abas)   
        call moldenwritemo(outunit, mo_o, mo_e, mo_c)
        close(outunit)
    end subroutine moldenwritefull


end module moldenwritemod
