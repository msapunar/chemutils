!--------------------------------------------------------------------------------------------------
! MODULE: MOOverlapMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date November, 2016
!
! DESCRIPTION: 
!> @brief Contains the subroutine for computing the overlap between two sets of molecular orbitals.
!--------------------------------------------------------------------------------------------------
module mooverlapmod
    use fortutils
    implicit none

    private
    public :: mooverlap, reducecsc
    public :: momatchphase


contains

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: MOOverlap
    !
    ! DESCRIPTION:
    !> @brief Compute the overlap (CSC) matrix between two sets of molecular orbitals.
    !> @details
    !! The overlap matrix between the basis functions of the two sets of molecular orbitals should
    !! be calculated in advance and passed to this subroutine.
    !! It is not required that the two sets of molecular orbitals are formed from the same basis
    !! functions (only that the correct S matrix is provided), contain the same number of orbitals,
    !! or that both sets are restricted/unrestricted.
    !----------------------------------------------------------------------------------------------
    subroutine mooverlap(mo1, mo2, s, csc)
        real(dp), intent(in) :: mo1(:, :, :) !< First set of molecular orbital coefficients.
                                             !! Dimensions :: nao1 x nmo1 x rhf1
        real(dp), intent(in) :: mo2(:, :, :) !< Second set of molecular orbital coefficients.
                                             !! Dimensions :: nao2 x nmo2 x rhf2
        real(dp), intent(in) :: s(:, :) !< Overlap matrix between atomic basis functions.
                                        !! Dimensions: nao1 x nao2
        real(dp), allocatable, intent(out) :: csc(:, : ,:) !< Overlap matrix.
                                        !! Dimensions: nmo1 x nmo2 x max(rhf1, rhf2)
        real(dp), allocatable :: temp(:, :)  !< Temporary for matrix multiplication.
        integer :: i, rhf

        if ((size(mo1, 1) /= size(s, 1)) .or. (size(mo2, 1) /= size(s, 2))) then
            write(stderr, *) 'Error in MOOverlap module, MOOverlap subroutine.'
            write(stderr, *) ' Number of basis functions: ', size(mo1, 1), size(mo2, 1)
            write(stderr, *) ' Smatrix dimensions: ', size(s,1), size(s,2)
            stop
        end if

        rhf = max(size(mo1, 3), size(mo2, 3))
        if (.not. allocated(csc)) allocate(csc(size(mo1, 2), size(mo2, 2), rhf))
        allocate(temp(size(mo1, 2), size(mo2, 1)))
        do i = 1, rhf
            temp = matmul(transpose(mo1(:, :, min(i, size(mo1,3)))), s)
            csc(:, :, i) = matmul(temp, mo2(:, :, min(i, size(mo2, 3))))
        end do
    end subroutine mooverlap


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: ReduceCSC
    !
    ! DESCRIPTION:
    !> @brief Remove inactive orbitals and order occupied/unoccupied orbitals in the csc matrix.
    !> @details
    !! Rows and coulmns corresponding to inactive orbitals are removed from the csc matrix. Rows
    !! and columns corresponding to occupied orbitals are placed at the beginning of the matrix.
    !----------------------------------------------------------------------------------------------
    subroutine reducecsc(om1, om2, am1, am2, csc, rcsc)
        logical, intent(in) :: om1(:, :) !< Occupied orbitals mask in set 1.
        logical, intent(in) :: om2(:, :) !< Occupied orbitals mask in set 2.
        logical, intent(in) :: am1(:) !< Active orbitals mask in set 1.
        logical, intent(in) :: am2(:) !< Active orbitals mask in set 2.
        real(dp), allocatable, intent(in) :: csc(:, :, :) !< Overlap matrix.
                                         !! Dimensions: nmo1 x nmo2 x max(rhf1, rhf2)
        real(dp), allocatable, intent(out) :: rcsc(:, :, :) !< Reduced overlap matrix.
                                         !! Dimensions: namo1 x namo2 x max(rhf1, rhf2)

        real(dp), allocatable :: tmat(:, :, :)
        integer :: i, s, no, nv

        allocate(tmat(count(am1), size(csc, 2), size(csc, 3)))
        if (.not. allocated(rcsc)) allocate(rcsc(count(am1), count(am2), size(csc, 3)))

        do s = 1, size(csc, 3)
            no = 0
            nv = count(om1(:, s) .and. am1) ! Index of last row of occupied orbitals.
            do i = 1, size(am1)
                if (.not. am1(i)) cycle
                if (om1(i, s)) then
                    no = no + 1
                    tmat(no, :, s) = csc(i, :, s)
                else
                    nv = nv + 1
                    tmat(nv, :, s) = csc(i, :, s)
                end if
            end do
            no = 0
            nv = count(om2(:, s) .and. am2) ! Index of last column of occupied orbitals.
            do i = 1, size(am2)
                if (.not. am2(i)) cycle
                if (om2(i, s)) then
                    no = no + 1
                    rcsc(:, no, s) = tmat(:, i, s)
                else
                    nv = nv + 1
                    rcsc(:, nv, s) = tmat(:, i, s)
                end if
            end do
        end do
    end subroutine reducecsc


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: MOMatchPhase
    !
    ! DESCRIPTION:
    !> @brief Adjust phase of a set of MOs to match the phase of reference set.
    !> @details
    !----------------------------------------------------------------------------------------------
    subroutine momatchphase(mo1, mo2, csc)
        use assignprobmod
        real(dp), intent(in) :: mo1(:, :, :) !< Reference set of molecular orbital coefficients.
                                             !! Dimensions :: nao1 x nmo1 x rhf1
        real(dp), intent(inout) :: mo2(:, :, :) !< Molecular orbital coefficients.
                                             !! Dimensions :: nao2 x nmo2 x rhf2

        real(dp), allocatable, intent(inout) :: csc(:, :, :) !< Overlap matrix.
                                       !! Dimensions: nmo1 x nmo2 x max(rhf1, rhf2)
        integer :: nmo1, nmo2, rhf2
        integer :: s
        integer :: j
        integer :: k
        integer :: cmatch(size(mo2, 2))

        nmo1 = size(mo1, 2)
        nmo2 = size(mo2, 2)
        rhf2 = size(mo2, 3)

        if ((nmo1 /= size(csc, 1)) .or. (nmo2 /= size(csc,2))) then
            write(stderr,*) 'Error in MOOverlap module, MOMatchPhase subroutine.'
            write(stderr,*) '  Array size mismatch.'
            stop
        end if
        if (nmo1 /= nmo2) then
            write(stderr,*) 'Error in MOOverlap module, MOMatchPhase subroutine.'
            write(stderr,*) ' nmo1 /= nmo2 not implemented'
            stop
        end if

        do s = 1, size(csc, 3)
            call assignprob(nmo2, -abs(csc(:, :, s)), cmatch)
            do j = 1, nmo2
                k = cmatch(j)
                if (csc(j, k, s) < 0) then
                    mo2(:, k, min(s, rhf2)) = -mo2(:, k, min(s, rhf2))
                    csc(:, k, s) = - csc(:, k, s)
                end if
            end do
        end do
    end subroutine momatchphase
     

end module mooverlapmod
