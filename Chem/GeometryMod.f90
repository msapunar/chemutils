!----------------------------------------------------------------------------------------------
! MODULE: GeometryMod
!> @author Marin Sapunar, Ruđer Bošković Institute
!> @date October, 2017
!
!----------------------------------------------------------------------------------------------
module geometrymod
    use fortutils
    implicit none

    private
    public :: geom_com
    public :: geom_len
    public :: geom_ang
    public :: geom_tors
    public :: geom_translation

contains


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: geom_com
    !> @brief Calculate center of mass.
    !----------------------------------------------------------------------------------------------
    pure function geom_com(mass, geom) result(res)
        real(dp), intent(in) :: mass(:)
        real(dp), intent(in) :: geom(:, :)
        real(dp) :: res(size(geom, 1))
        res = matmul(geom, mass) / sum(mass)
    end function geom_com


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: geom_len
    !> @brief Calculate distance between two points from a list.
    !----------------------------------------------------------------------------------------------
    pure function geom_len(geom, ind) result(res)
        real(dp), intent(in) :: geom(:, :) !< List. Dimensions: ndim x nat.
        integer, intent(in) :: ind(2) !< Index of selected points.
        real(dp) :: vec(size(geom, 1)) !< Vector connecting selected points.
        real(dp) :: res !< Distance.
        vec = geom(:, ind(1)) - geom(:, ind(2))
        res = sqrt(dot_product(vec, vec))
    end function geom_len

    
    !----------------------------------------------------------------------------------------------
    ! FUNCTION: geom_ang
    !> @brief Calculate angle between three points from a list.
    !----------------------------------------------------------------------------------------------
    pure function geom_ang(geom, ind) result(res)
        real(dp), intent(in) :: geom(:, :) !< List. Dimensions: ndim x nat.
        integer, intent(in) :: ind(3) !< Index of selected points.
        real(dp) :: v1(size(geom, 1))
        real(dp) :: v2(size(geom, 1))
        real(dp) :: res
        v1 = geom(:, ind(3)) - geom(:, ind(2))
        v2 = geom(:, ind(1)) - geom(:, ind(2))
        res = acos(dot_product(v1, v2) / sqrt(dot_product(v1, v1)) / sqrt(dot_product(v2, v2)))
    end function geom_ang


    !----------------------------------------------------------------------------------------------
    ! FUNCTION: geom_tors
    !> @brief Calculate torsional angle between four points from a list.
    !----------------------------------------------------------------------------------------------
    pure function geom_tors(geom, ind) result(res)
        use mathmod, only : cross
        real(dp), intent(in) :: geom(:, :) !< List. Dimensions: ndim x nat.
        integer, intent(in) :: ind(4) !< Index of selected points.
        real(dp) :: v1(size(geom, 1))
        real(dp) :: v2(size(geom, 1))
        real(dp) :: v3(size(geom, 1))
        real(dp) :: x
        real(dp) :: y
        real(dp) :: res
        v1 = geom(:, ind(2)) - geom(:, ind(1))
        v2 = geom(:, ind(3)) - geom(:, ind(2))
        v3 = geom(:, ind(4)) - geom(:, ind(3))
        v1 = cross(v1, v2)
        v3 = cross(v2, v3)
        y = dot_product(cross(v1, v3), v2/sqrt(dot_product(v2, v2)))
        x = dot_product(v1, v3)
        res = atan2(y, x)
    end function geom_tors


    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: geom_translation
    !> @brief Translate (subset) of points along a given direction.
    !----------------------------------------------------------------------------------------------
    subroutine geom_translation(geom, vec, d, subset)
        real(dp), intent(inout) :: geom(:, :) !< List of points. Dimensions: ndim x nat.
        real(dp), intent(in) :: vec(:) !< Direction. Dimensions: ndim.
        real(dp), intent(in) :: d !< Distance.
        integer, intent(in), optional :: subset(:) !< Subset of points to be translated.
        real(dp) :: u(size(vec, 1))
        integer, allocatable :: sel(:)
        integer :: i

        if (present(subset)) then
            allocate(sel, source=subset)
        else
            allocate(sel(size(geom, 2)))
            sel = [(i, i = 1, size(geom, 2))]
        end if

        u = vec / sqrt(dot_product(vec, vec))
        do i = 1, size(sel)
            geom(:, sel(i)) = geom(:, sel(i)) + d * u
        end do
    end subroutine geom_translation


end module geometrymod
