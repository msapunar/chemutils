!--------------------------------------------------------------------------------------------------
! MODULE: AtombasisType
!
! DESCRIPTION: 
!> @brief Contains the AtomBasis type and accompanying subroutines.
!--------------------------------------------------------------------------------------------------
module atombasistype
    ! Import variables
    use fortutils
    implicit none
 
    private
    public :: atombasis

    !----------------------------------------------------------------------------------------------
    ! TYPE: AtomBasis
    !
    ! DESCRIPTION:
    !> @brief Contains all information about the basis set for a single atom.
    !----------------------------------------------------------------------------------------------
    type atombasis
        character(len=:), allocatable :: key !< Name of the basis set for the atom.
        integer :: nfunc = 0 !< Number of basis functions for the atom.
        integer :: ncart = 0 !< Number of cartesian basis functions for the atom.
        integer :: nsphe = 0 !< Number of spherical basis functions for the atom.
        integer, allocatable :: l(:) !< Angular momenta of the functions.
        character, allocatable :: typ(:) !< Types (angular momenta) of the functions.
        integer, allocatable :: npr(:) !< Number of primitive functions per contracted function.
        type(rvec), allocatable :: z(:) !< Zeta coefficients.
        type(rvec), allocatable :: b(:) !< Beta coefficients.
    contains
        procedure :: normalize
    end type atombasis


contains

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: Normalize
    !
    ! DESCRIPTION:
    !> @brief Normalize the contracted Gaussian functions.
    !> @details 
    !! Multiply contraction coefficients of each contracted basis function by
    !!   the norm (N) of the full contraction.
    !!   b'(i) = N * b(i)
    !! After expanding the basis set by angular momenta, the coefficients need
    !!   to be multiplied by the norms of the individual primitive Gaussian 
    !!   functions for the basis set to be fully normalized.
    !----------------------------------------------------------------------------------------------
    subroutine normalize(self)
        class(atombasis), intent(inout) :: self !< AtomBasis variable.
        integer:: i
        integer:: j
        integer:: k
        integer:: l
        real(dp):: b1
        real(dp):: b2
        real(dp):: z1
        real(dp):: z2
        real(dp):: p1
        real(dp):: p2
        real(dp):: nsum
        real(dp):: norm
        
        if (self%nfunc == 0) then 
            write(stderr,*) 'Error in AtomBasis type, Normalize subroutine.'
            write(stderr,*) ' Number of basis functions not defined.'
            stop
        end if
        do i = 1, self%nfunc
            nsum = 0.0_dp
            select case (self%typ(i))
                case('s')
                    l = 0
                case('p')
                    l = 1
                case('d')
                    l = 2
                case('f')
                    l = 3
                case('g')
                    l = 4
                case('h')
                    l = 5
            end select
            p1=3.0_dp/4.0_dp+l/2.0_dp
            p2=3.0_dp/2.0_dp+l
            do j = 1, self%npr(i)
                b1 = self%b(i)%c(j)
                z1 = self%z(i)%c(j)
                do k = 1, self%npr(i)
                    b2 = self%b(i)%c(k)
                    z2 = self%z(i)%c(k)
                    nsum = nsum + b1*b2 * z1**p1 * z2**p1 / ((z1+z2)**p2)
                end do
            end do
            norm = 2**(-p1) / sqrt(nsum)
            self%b(i)%c = self%b(i)%c * norm
        end do
    end subroutine normalize


end module atombasistype
