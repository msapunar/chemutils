module sortmod
    use fortutils
    implicit none

contains

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: SelectionSort
    !> @brief In place selection sort of a list of reals.
    !----------------------------------------------------------------------------------------------
    subroutine selectionsort(a)
        real(dp), intent(inout) :: a(:) !< List to be sorted.
        integer :: minIndex
        integer :: i
        real(dp) :: temp

        do i = 1, size(a)-1
            minIndex = minloc(a(i:), 1) + i - 1
            if (a(i) > a(minIndex)) then
                temp = a(i)
                a(i) = a(minIndex)
                a(minIndex) = temp
            end if
        end do
    end subroutine selectionsort


end module sortmod
