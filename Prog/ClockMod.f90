module clockmod
    use fortutils
    implicit none

    private
    public :: clocktype

    !----------------------------------------------------------------------------------------------
    ! TYPE: ClockType
    !
    ! DESCRIPTION:
    !> @brief Object for measuring run time.
    !> @details 
    !! The Start procedure sets the internal state of the clock. The Print procedure outputs the
    !! time since the last call of the Start procedure.
    !! Example use:
    !!
    !!     type(clocktype) :: clock
    !!     call clock%start()
    !!     ...
    !!     do_something
    !!     ...
    !!     call clock%print(stdout, message)
    !!
    !----------------------------------------------------------------------------------------------
    type clocktype
        integer :: t0 = 0 !< Time when clock was started.
    contains
        procedure :: start => cstart !< Start the clock.
        procedure :: print => cprint !< Print time since last clock update.
    end type clocktype

contains

    
    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: CStart
    !> @brief Initialize the clock.
    !----------------------------------------------------------------------------------------------
    subroutine cstart(self)
        class(clocktype) :: self
        call system_clock(self%t0)
    end subroutine cstart

    !----------------------------------------------------------------------------------------------
    ! SUBROUTINE: CPrint
    !> @brief Print the current state of the clock.
    !> @details
    !! Time since the last update of the clock is printed in days/hours/minutes/seconds. The output
    !! is printed to outunit, preceeded by a message passed to the subroutine.
    !----------------------------------------------------------------------------------------------
    subroutine cprint(self, outunit, msg)
        class(clocktype), intent(in) :: self
        integer, intent(in) :: outunit
        character(len=*), intent(in) :: msg
        integer :: now
        integer :: rate
        integer :: d, h, m
        real(dp) :: s
        call system_clock(now, rate)
        s = (now - self%t0) / real(rate, kind=dp)
        d = floor(s/86400.0_dp)
        s = s - d * 86400.0_dp
        h = floor(s/3600.0_dp)
        s = s - h * 3600.0_dp
        m = floor(s/60.0_dp)
        s = s - m * 60.0_dp
        write(outunit, 1000) msg, d, ' days, ', h, ' hours, ', m, ' minutes, ', s, ' seconds.'

1000 format (a,1x,i0,a,i0,a,i0,a,f5.2,a)

    end subroutine cprint


end module clockmod
