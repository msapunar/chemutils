module sparsemod
    use fortutils
    implicit none

    type sparsev
        integer :: n = 0 !< Number of non-zero elements.
        integer, allocatable :: i(:) !< Indexes.
        real(dp), allocatable :: v(:) !< Values.
    end type sparsev

contains

    pure function sparse_dot(v1, v2) result(dot)
        type(sparsev), intent(in) :: v1
        type(sparsev), intent(in) :: v2
        real(dp) :: dot
        integer :: i, j

        dot = 0.0_dp
        do i = 1, v1%n
            do j = 1, v2%n
                if (v1%i(i) /= v2%i(j)) cycle
                dot = dot + v1%v(i) * v2%v(j)
            end do
        end do
    end function sparse_dot

end module sparsemod
